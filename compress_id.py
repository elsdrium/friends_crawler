#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys


def compress(s):
    return s.replace('@N', '')[:9]


if __name__ == '__main__':
    edges = set()
    vmap = {}
    with open(sys.argv[1], 'r') as f:
        while True:
            e = tuple(f.readline().replace('\n', '').split(' '))
            if len(e) < 2:
                break
            edges.add(e)
            vmap[e[0]] = compress(e[0])
            vmap[e[1]] = compress(e[1])

    with open(sys.argv[1]+'.out', 'w') as f:
        f.write('\n'.join([vmap[v1] + ' ' + vmap[v2] for v1, v2 in edges]))
