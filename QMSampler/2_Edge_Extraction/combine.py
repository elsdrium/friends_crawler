import sys
import networkx as nx

with open(sys.argv[1], "rb") as f1:
    edgeList1 = f1.read().splitlines()


with open(sys.argv[2], "rb") as f2:
    edgeList2 = f2.read().splitlines()

G1 = nx.read_edgelist(edgeList1)
G2 = nx.read_edgelist(edgeList2)

H = nx.compose(G1,G2)
print "Number of vertices after combining two reparsed graphs: ", len(list(H.nodes()))
print "Number of edges after combining two reparsed graphs: ", len(list(H.edges()))
fMerge = open(sys.argv[3],'wb')
nx.write_edgelist(H, fMerge, data=False)
fMerge.close()
