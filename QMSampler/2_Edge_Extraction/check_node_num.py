import sys
import networkx as nx

with open(sys.argv[1]+'.reparsed', "rb") as f1:
    edgeList1 = f1.read().splitlines()
    
with open(sys.argv[2]+'.reparsed', "rb") as f2:
    edgeList2 = f2.read().splitlines()
    
G1 = nx.read_edgelist(edgeList1)
G2 = nx.read_edgelist(edgeList2)

print("Number of vertices sampled from DBLP after reparsed again: ", len(list(G1.nodes())))
print("Number of edges sampled from DBLP after reparsed again: ", len(list(G1.edges())))

print("Number of vertices sampled from MS after reparsed again: ", len(list(G2.nodes())))
print("Number of edges sampled from MS after reparsed again: ", len(list(G2.edges())))

current_node_info1 = open('current_node_info_of_'+sys.argv[1]+'.reparsed','wb')
current_node_info1.write( str( len( list( G1.nodes() ) ) ) )
current_node_info1.close()

current_node_info2 = open('current_node_info_of_'+sys.argv[2]+'.reparsed','wb')
current_node_info2.write( str( len( list( G2.nodes() ) ) ) )
current_node_info2.close()