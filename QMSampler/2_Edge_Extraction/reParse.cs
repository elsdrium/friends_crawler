﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;

namespace reParse
{
    class reParse
    {
        static void warning(string msg)
        {
            Console.WriteLine("[Warning] " + msg);
        }

        static void status(string msg)
        {
            Console.WriteLine("[STATUS] " + msg);
        }
        static void Main(string[] args)
        {
            if(args.Length<4)
            {
                Console.WriteLine("Usage: reParse WholeGraph outFilePrefix N inFileName1 inFileName2 ... inFileNameN");
                return;
            }

            int inFileCnt =0;
            if(Int32.TryParse(args[2],out inFileCnt)==false)
            {
                Console.WriteLine("Usage: reParse WholeGraph outFilePrefix N inFileName1 inFileName2 ... inFileNameN");
                return;
            }
            string [] inFileName = new string[inFileCnt];
            for (int i = 0; i < inFileCnt; i++)
                inFileName[i] = args[3 + i];

            try
            {
                //read sampled nodes
                HashSet<long> [] nodes = new HashSet<long>[inFileName.Length];
                string line;
                string[] tokens;
                long first, second;
                StreamReader sr;
                for (int i = 0; i < inFileName.Length; i++)
                {
                    status("reading in node file #"+(i+1)+"  FileName = "+inFileName[i]);
                    sr = new StreamReader(inFileName[i]);
                    nodes[i] = new HashSet<long>();
                    
                    while ((line = sr.ReadLine()) != null)
                    {
                        tokens = line.Split(new char[] { ' ', ',', '\t' }, StringSplitOptions.RemoveEmptyEntries);
                        if (tokens.Length != 2)
                        {
                            warning("skip line = " + line);
                            continue;
                        }

                        
                        if (Int64.TryParse(tokens[0], out first) == false || Int64.TryParse(tokens[1], out second) == false)
                        {
                            warning(" int parsing error! skip line = " + line);
                            continue;
                        }

                        if (!nodes[i].Contains(first))
                            nodes[i].Add(first);
                        if (!nodes[i].Contains(second))
                            nodes[i].Add(second);
                    }
                    status("inFile #" + (i + 1) + " contains " + nodes[i].Count() + " different nodes.");
                    sr.Close();
                }
                //read whole graph
                sr = new StreamReader(args[0]);
                status("reading whole graph file... FileName = "+args[0]);

                StreamWriter[] sw = new StreamWriter[inFileName.Length];
                for(int i=0;i<inFileName.Length;i++)
                    sw[i] = new StreamWriter(inFileName[i]+".reparsed");
                int curLine = 0;
                while ((line = sr.ReadLine()) != null)
                {
                    curLine++;
                    if (curLine % 200000 == 0)
                        status("Line " + curLine + " of whole graph line has loaded.");
                    tokens = line.Split(new char[] { ' ', ',', '\t' }, StringSplitOptions.RemoveEmptyEntries);
                    if (tokens.Length != 2)
                    {
                        warning("skip line = " + line);
                        continue;
                    }

                    if (Int64.TryParse(tokens[0], out first) == false || Int64.TryParse(tokens[1], out second) == false)
                    {
                        warning("skip line = " + line);
                        continue;
                    }

                    //key point: nodes[i] ==> the sampled node set of input i-th file 
                    //(first,second) is an edge in the whole graph file
                    //so, for node[i] is a pool of nodes. Here we are thying to add edges among them by looking up the edges
                    //in the whole graph file
                    for (int i = 0; i < nodes.Length; i++)
                    {
                        if (nodes[i].Contains(first) && nodes[i].Contains(second))
                            sw[i].WriteLine(first + "\t" + second);
                    }
                }
                sr.Close();
                for (int i = 0; i < inFileName.Length; i++)
                    sw[i].Close();


            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message + "\n" + e.StackTrace);
            }
        }
    }
}
