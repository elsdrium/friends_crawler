#!bin/bash

cd 2_Edge_Extraction
mono reParse.exe ../MS_DBLP_Dataset/DBLP.csv iter0_dblp 1 ../1_SampleConsole/iter0_dblp_MHRW_10k.0
mono reParse.exe ../MS_DBLP_Dataset/MS.csv iter0_ms 1 ../1_SampleConsole/iter0_ms_MHRW_10k.0
cd ../3_SingleNetwork_DegSigma
mono SingleNetwork_avgDeg_Sigma.exe ../1_SampleConsole/iter0_dblp_MHRW_10k.0.reparsed > avg_deg_and_var_of_DBLP_10000_iter0.txt
mono SingleNetwork_avgDeg_Sigma.exe ../1_SampleConsole/iter0_ms_MHRW_10k.0.reparsed > avg_deg_and_var_of_MS_10000_iter0.txt
cd ../4_MatchMerge
mono MatchMerge.exe ../MS_DBLP_Dataset/matchList.dat ../1_SampleConsole/iter0_dblp_MHRW_10k.0.reparsed ../1_SampleConsole/iter0_ms_MHRW_10k.0.reparsed 90000000 Merged_20000_iter0.txt
cd ../5_Matched_DegSigma
mono DA_matched_AvgDeg_Sigma.exe ../4_MatchMerge/Merged_20000_iter0.txt > match_avg_deg_and_var_20000_iter0.txt
cd ../6_QMSampler
python QMSamplerInput_20000_iter0.py
./QMSampler < QMSamplerInput_20000_iter0.txt
python next_num_node_and_edge.py 20000 ../1_SampleConsole/iter0_dblp_MHRW_10k.0.reparsed ../1_SampleConsole/iter0_ms_MHRW_10k.0.reparsed num_node_dblp_20000_iter1.txt num_node_ms_20000_iter1.txt

cd ..
cp 1_SampleConsole/iter0_dblp_MHRW_10k.0.reparsed 2_Edge_Extraction/Merged_dblp_iter0.reparsed
cp 1_SampleConsole/iter0_ms_MHRW_10k.0.reparsed 2_Edge_Extraction/Merged_ms_iter0.reparsed
