import sys

Size_1 = sys.argv[1]
Size_2 = sys.argv[2]
crawled_node_1 = sys.argv[3]
crawled_node_2 = sys.argv[4]

with open('../3_SingleNetwork_DegSigma/avg_deg_and_var_of_foursquare','r') as f1:
    x = f1.read().split()
    
avg_deg_dblp = x[4]
var_dblp = x[5]

with open('../3_SingleNetwork_DegSigma/avg_deg_and_var_of_flickr','r') as f2:
    y = f2.read().split()
    
avg_deg_ms = y[4]
var_ms = y[5]

with open('../5_Matched_DegSigma/match_avg_deg_and_var_edge_list','r') as f3:
    z = f3.read().split()

match_num_outNode = z[6]
match_var = z[8]

target_node = int(sys.argv[5])
epsilon = float(sys.argv[6])

fout = open('QMSamplerInput.txt','w')
fout.write(str(Size_1)+' '+str(crawled_node_1)+' '+str(avg_deg_dblp)+' '+str(var_dblp)+' '\
          +str(Size_2)+' '+str(crawled_node_2)+' '+str(avg_deg_ms)+' '+str(var_ms)+' '\
          +str(match_num_outNode)+' '+str(match_var)+' '+str(target_node)+' '+str(epsilon))