//=======================================================================
// Copyright (C) 2003-2013 William Hallahan
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without restriction,
// including without limitation the rights to use, copy, modify, merge,
// publish, distribute, sublicense, and/or sell copies of the Software,
// and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//=======================================================================

// PolynomialTest.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include <vector>
#include "Polynomial.h"
#include <cstdlib>
#include <fstream>
#include <cstdlib>
#include <cmath>

using namespace std;

void DisplayPolynomial(const Polynomial & polynomial);
void GetPolynomial(Polynomial & polynomial, int polynomial_type);
float w1;
float w2;
float w3;
float tau;
float u;
float v;
float w;
int n;
//======================================================================
//  Start of main program.
//======================================================================

int main(int argc, char* argv[])
{
    //------------------------------------------------------------------
    //  Get the type of test.
    //------------------------------------------------------------------

/*
Threshold                //threshold, floating point number
Quality                     //required quality, floating point number
N1 	                        //填ACM sample的數量
x  	                        //ACM sample到的點的degree variance
avg	                        //ACM sample到的點的averge degree
N2 	                        //填DBLP sample的數量
x  	                        //DBLP sample到的點的degree variance
avg	                       //DBLP sample到的點的average degree */


  //float threshold;
  //float quality;
  float n1,sigma1,avg1,n2,sigma2,avg2,n3,sigma3;
 // std::cin>>  n1 >> sigma1 >> avg1 >> n2 >> sigma2 >> avg2 >> n3>> sigma3;
 // fout<<threshold << quality << n1 << sigma1 << avg1 << n2 << sigma2 << avg2;

float N1=45000000;
float N2=700000000;
float epsilon=0.f;

    std::cout << std::endl;
    std::cout << "Enter the size of network 1:" << std::endl;
    std::cin >> N1;
    std::cout << "Enter the number of crawled nodes in network 1:" << std::endl;
    std::cin >> n1;
    std::cout << "Enter the average of network 1:" << std::endl;
    std::cin >> avg1;
    std::cout << "Enter the variance of network 1:" << std::endl;
    std::cin >> sigma1;
    std::cout << std::endl;
    std::cout << "Enter the size of network 2:" << std::endl;
    std::cin >> N2;
    std::cout << "Enter the number of crawled nodes in network 2:" << std::endl;
    std::cin >> n2;
    std::cout << "Enter the average of network 2:" << std::endl;
    std::cin >> avg2;
    std::cout << "Enter the variance of network 2:" << std::endl;
    std::cin >> sigma2;
    std::cout << std::endl;
    std::cout << "Enter the number of matched nodes:" << std::endl;
    std::cin >> n3;
    std::cout << "Enter the variance of matched nodes:" << std::endl;
    std::cin >> sigma3;
    std::cout << "Enter the target size:" << std::endl;
    std::cin >> n;
    std::cout << "Enter the threshold epsilon:" << std::endl;
    std::cin >> epsilon;

float N3=n3*sqrt((N1/n1)*(N2/n2));

w1=(N1-N3)/(N1+N2-N3);
w2=(N2-N3)/(N1+N2-N3);
w3=1-w1-w2;
std::cout<<"Initial: "<<std::endl;
std::cout<<"n1: "<<n1<<std::endl;
std::cout<<"n2: "<<n2<<std::endl;
std::cout<<"n3: "<<n3<<std::endl;
tau=0.9;
float tauc=1-tau;
int nn=n1+n2;
float sigmas=(pow((w1*sigma1/(n1-n3)),2)*(1-(n1-n3-1)/(N1-N3-1))+pow((w2*sigma2/(n2-n3)),2)*(1-(n2-n3-1)/(N2-N3-1))+pow((w3*sigma3/(n3)),2)*(1-(n3-1)/(N3-1)));
float sigman=(1/(nn-1))*tauc*((N1-N3)*n1*(avg2-avg1)*(avg2-avg1)/N1+(N2-N3)*n2*(avg2-avg1)*(avg2-avg1)/N2);
float qualitynow=1.0-(1/(epsilon*epsilon))*(sigmas+sigman);
std::cout<<"qualitynow = "<<qualitynow<<std::endl;

u=(w1*sigma1*N1/(tau*(N1-N3)))*(w1*sigma1*N1/(tau*(N1-N3)));
v=(w2*sigma2*N2/(tau*(N2-N3)))*(w2*sigma2*N2/(tau*(N2-N3)));
w=(1-tau)*(avg2-avg1)*(avg2-avg1)*((N1-N3)/N1-(N2-N3)/N2)/(n-1);


//cout<<"w123: "<<w1<<","<<w2<<","<<w3<<endl;

//cout<<"u: "<<u<<"v: "<<v<<"c: "<<c<<"D:"<<D<<endl<<"This one: "<<0.75*n*n-D+pow(n,3)/D<<endl;



    /*std::cout << std::endl;
    std::cout << "1. Find roots of the polynomial." << std::endl;
    std::cout << "2. Evaluate the polynomial at a real value" << std::endl;
    std::cout << "3. Evaluate the polynomial and its derivative at a real value" << std::endl;
    std::cout << "4. Evaluate the polynomial at a complex value" << std::endl;
    std::cout << "5. Evaluate the polynomial and its derivative at a complex value" << std::endl;
    std::cout << "6. Test polynomial arithmetic." << std::endl;
    std::cout << "7. Test polynomial division." << std::endl;
    std::cout << std::endl;
    std::cout << "Enter the type of test > ";
    int test_type;
    std::cin >> test_type;*/

    //------------------------------------------------------------------
    //  Get the type of polynomial.
    //------------------------------------------------------------------
    int test_type = 1;
    /*std::cout << "1. Arbitrary polynomial" << std::endl;
    std::cout << "2. Polynomial with maximum power and scalar value 1.0, the rest 0.0." << std::endl;
    std::cout << "3. Polynomial with  all coefficient equal to 1.0." << std::endl;
    std::cout << std::endl;
    std::cout << "Enter the type of polynomial > ";
    int polynomial_type;
    std::cin >> polynomial_type;
    std::cout << std::endl;*/
    int initialn1=n1;
    int initialn2=n2;
float nn3;
    //------------------------------------------------------------------
    //  Get a polynomial.
    //------------------------------------------------------------------
    int polynomial_type = 1;
    Polynomial polynomial;

    GetPolynomial(polynomial, polynomial_type);

            std::vector<double> real_vector;
            std::vector<double> imag_vector;
            std::vector<int> root;

            int degree = polynomial.Degree();

            real_vector.resize(degree);
            imag_vector.resize(degree);

            double * real_vector_ptr = &real_vector[0];
            double * imag_vector_ptr = &imag_vector[0];

            int root_count= 0;

            if (polynomial.FindRoots(real_vector_ptr,
                                     imag_vector_ptr,
                                     &root_count) == PolynomialRootFinder::SUCCESS)
            {
                int i = 0;

                for (i = 0; i < root_count; i++)
                {
                    //std::cout << "Root " << i << " = " << real_vector_ptr[i] << " + i " << imag_vector_ptr[i] << std::endl;

                    if (imag_vector_ptr[i]==0 && real_vector_ptr[i]>0){
                            //cout<<real_vector_ptr[i]<<std::endl;
                            root.push_back(int(real_vector_ptr[i]));
                    }
                }
            }
            else
            {
                //std::cout << "Failed to find all roots." << std::endl;
            }

/*int tempn1,tempn2;
for(int i=0;i<root.size();i++)

    cout<<root[0]<<std::endl;*/



float u1=(w1*sigma1*N1/(tau*(N1-N3)))*(w1*sigma1*N1/(tau*(N1-N3)));
float v1=(1-tau)*(N1-N3)*avg2*avg2/N1;
float c1=2*u1*n*n/v1;
float D1=n*n/4,D2=((float)pow((float)((float)sqrt((float)3)*(float)sqrt((float)27*(float)pow((float)n,4)*c1*c1-256*(float)pow((float)c1,3))+9*n*n*c1),1/3))/((float)pow(float(2),1/3)*(float)pow(float(3),2/3)),D3=4*((float)pow(float(2/3),1/3)*c1)/((float)pow((float)((float)sqrt((float)3)*(float)sqrt((float)27*(float)pow((float)n,4)*c1*c1-256*(float)pow((float)c1,3))+9*n*n*c1),1/3));
float D=D1+D2+D3;
//cout<<"w123: "<<w1<<","<<w2<<","<<w3<<endl;

//cout<<"u: "<<u<<"v: "<<v<<"c: "<<c<<"D:"<<D<<endl<<"This one: "<<0.75*n*n-D+pow(n,3)/D<<endl;
  int out1=0;
  int out2=0;
  float tempq=0.f,optq=0.f;

if (root.size()>0){
  for(int i=0;i<root.size();i++){
    out1=root[i];
    if(out1>0 && out1<n){
       //std::cout << "i:" << i <<"out1" << out2 << std::endl;
       out2=n-out1;
       nn3=N3*sqrt((out1/N1)*(out2/N2));
       sigmas=(pow((w1*sigma1/(out1-nn3)),2)*(1-(out1-nn3-1)/(N1-N3-1))+pow((w2*sigma2/(out2-nn3)),2)*(1-(out2-nn3-1)/(N2-N3-1))+pow((w3*sigma3/(nn3)),2)*(1-(nn3-1)/(N3-1)));
       sigman=(1/(n-1))*tauc*((N1-N3)*out1*(avg2-avg1)*(avg2-avg1)/N1+(N2-N3)*out2*(avg2-avg1)*(avg2-avg1)/N2);
       tempq=1.0-(1/(epsilon*epsilon))*(sigmas+sigman);

       if(tempq>optq){
        n1=out1;
        n2=out2;
        optq=tempq;
        //std::cout <<"optq:"<<optq<<std::endl;
       }
    }
  }
}
std::cout<<"Sextic: "<<std::endl;
std::cout<<"n1: "<<n1<<std::endl;
std::cout<<"n2: "<<n2<<std::endl;
std::cout<<"nn3: "<<nn3<<std::endl;
std::cout<<"optq: "<<optq<<std::endl;
std::cout <<"------------------"<<std::endl;

u=(w1*sigma1*N1/(tau*(N1-N3)))*(w1*sigma1*N1/(tau*(N1-N3)));
v=(1-tau)*(N1-N3)*avg2*avg2/N1;
float c=2*u*n*n/v;
D1=n*n/4,D2=((float)pow((float)((float)sqrt((float)3)*(float)sqrt((float)27*(float)pow((float)n,4)*c*c-256*(float)pow((float)c,3))+9*n*n*c),1/3))/((float)pow(float(2),1/3)*pow(float(3),2/3)),D3=4*((float)pow(float(2/3),1/3)*c)/(pow(((float)sqrt((float)3)*(float)sqrt((float)27*(float)pow((float)n,4)*c*c-256*(float)pow((float)c,3))+9*n*n*c),1/3));
D=D1+D2+D3;
//cout<<"w123: "<<w1<<","<<w2<<","<<w3<<endl;

//cout<<"u: "<<u<<"v: "<<v<<"c: "<<c<<"D:"<<D<<endl<<"This one: "<<0.75*n*n-D+pow(n,3)/D<<endl;
out1= floor(0.5*sqrt(D)-0.5*sqrt(0.75*n*n+D+(float)pow((float)n,3)/D)+n/4);
out2;

if(out1<0||out1>n)
{
  out1=n*w1;
  out2=n-out1;
}
else if(out1<n && out1>0){
  //   cout<<"here out1:"<<out1<<endl;
     out2=n-out1;
}

/*int out1= floor(0.5*sqrt(D)-0.5*sqrt(0.75*n*n+D+pow(n,3)/D)+n/4);
int out2;

if(out1<0||out1>n)
{ cout<<out1<<endl;
  out1=n*w1;
  cout<<out1<<endl;
  out2=n-out1;
}
else if(out1<n && out1>0){
  //   cout<<"here out1:"<<out1<<endl;
     out2=n-out1;
}*/


nn3=N3*sqrt((out1/N1)*(out2/N2));
sigmas=(pow((w1*sigma1/(out1-nn3)),2)*(1-(out1-nn3-1)/(N1-N3-1))+pow((w2*sigma2/(out2-nn3)),2)*(1-(out2-nn3-1)/(N2-N3-1))+pow((w3*sigma3/(nn3)),2)*(1-(nn3-1)/(N3-1)));
sigman=(1/(n-1))*tauc*((N1-N3)*out1*(avg2-avg1)*(avg2-avg1)/N1+(N2-N3)*out2*(avg2-avg1)*(avg2-avg1)/N2);
tempq=1.0-(1/(epsilon*epsilon))*(sigmas+sigman);
std::cout<<"Fss: "<<std::endl;
std::cout<<"out1: "<<out1<<std::endl;
std::cout<<"out2: "<<out2<<std::endl;
std::cout<<"nn3: "<<nn3<<std::endl;
std::cout<<"tempq: "<<tempq<<std::endl;

if (tempq>optq){
        n1=out1;
        n2=out2;
        optq=tempq;
        //std::cout <<"optq:"<<optq<<std::endl;
}

    //------------------------------------------------------------------
    //  Perform different processing for the different tests.
    //------------------------------------------------------------------
            //----------------------------------------------------------
            //  Find the roots of the polynomial.
            //----------------------------------------------------------
if (n1<initialn1){
    n1=initialn1;
    n2=n-n1;
}

else if (n2 < initialn2){
    n2=initialn2;
    n1=n-n2;
}

if (qualitynow <0){
    qualitynow=0;
}
if(n1<0 || n2<0){
	n1 = n*abs(n1)/(abs(n1)+abs(n2));
	n2 = n*abs(n2)/(abs(n1)+abs(n2));
}

nn3=N3*sqrt((n1/N1)*(n2/N2));
std::cout<<"Final: "<<std::endl;
std::cout<<"n1: "<<n1<<std::endl;
std::cout<<"n2: "<<n2<<std::endl;
std::cout<<"nn3: "<<nn3<<std::endl;
sigmas=(pow((w1*sigma1/(n1-nn3)),2)*(1-(n1-nn3-1)/(N1-N3-1))+pow((w2*sigma2/(n2-nn3)),2)*(1-(n2-nn3-1)/(N2-N3-1))+pow((w3*sigma3/(nn3)),2)*(1-(nn3-1)/(N3-1)));
sigman=(1/(n-1))*tauc*((N1-N3)*out1*(avg2-avg1)*(avg2-avg1)/N1+(N2-N3)*out2*(avg2-avg1)*(avg2-avg1)/N2);
float finalq=1.0-(1/(epsilon*epsilon))*(sigmas+sigman);
std::cout<<"finalq: "<<finalq<<std::endl;

if (finalq <0){
    finalq=0;
}

ofstream fout("output.txt");
fout<<"Size of network 1: "<<n1<<" Size of network 2: "<<n2<<'\n';
fout<<"Possibility now: "<< qualitynow <<'\n';
fout<<"Estimated final possibility: "<< finalq <<'\n';
fout.close();

//system("pause");
	return 0;
}

//======================================================================
//  Function to display a polynomial.
//======================================================================

void DisplayPolynomial(const Polynomial & polynomial)
{

    int power = 0;

    for (power = polynomial.Degree(); power > 0; --power)
    {
        //--------------------------------------------------------------
        //  Display the coefficient if it is not equal to one.
        //--------------------------------------------------------------

        if (polynomial[power] != 1.0)
        {
            std::cout << polynomial[power];
        }

        //--------------------------------------------------------------
        //  If this is not the scalar value, then display the variable
        //  X.
        //--------------------------------------------------------------

        if (power > 0)
        {
            std::cout << " X";
        }

        //--------------------------------------------------------------
        //  If this is higher than the first power, then display the
        //  exponent.
        //--------------------------------------------------------------

        if (power > 1)
        {
            std::cout << "^" << power;
        }

        //--------------------------------------------------------------
        //  Add each term together.
        //--------------------------------------------------------------

        std::cout << " + ";
    }

    //------------------------------------------------------------------
    //  Display the polynomial's scalar value.
    //------------------------------------------------------------------

    std::cout << polynomial[power] << std::endl;

    return;
}

//======================================================================
//  Function: GetPolynomial
//======================================================================

void GetPolynomial(Polynomial & polynomial, int polynomial_type)
{
    //------------------------------------------------------------------
    //  Get the polynomial degree.
    //------------------------------------------------------------------

    //std::cout << "Enter the polynomial degree > ";
    int degree = 6;
    //std::cin >> degree;
    //std::cout << std::endl;

    //------------------------------------------------------------------
    //  Create a buffer to contain the polynomial coefficients.
    //------------------------------------------------------------------

    std::vector<double> coefficient_vector;

    coefficient_vector.resize(degree + 1);

    double * coefficient_vector_ptr = &coefficient_vector[0];

    //------------------------------------------------------------------
    //  Create the specified type of polynomial.
    //------------------------------------------------------------------

    int i = 0;

        //--------------------------------------------------------------
        //  Create an arbitrary polynomial.
        //--------------------------------------------------------------
        // Modify the script here
        coefficient_vector_ptr[6]=w;
        coefficient_vector_ptr[5]=-3*w*n;
        coefficient_vector_ptr[4]=3*w*n*n;
        coefficient_vector_ptr[3]=-1*(2*v+2*u+w*n*n*n);
        coefficient_vector_ptr[2]=6*u*n;
        coefficient_vector_ptr[1]=-6*u*n*n;
        coefficient_vector_ptr[0]=2*u*n*n*n;

    //------------------------------------------------------------------
    //  Create an instance of class Polynomial.
    //------------------------------------------------------------------

    polynomial.SetCoefficients(coefficient_vector_ptr, degree);

    return;
}
