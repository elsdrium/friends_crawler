import sys
import networkx as nx

with open('output.txt','r') as f:
	x = f.read().split()

target_node_num = sys.argv[1]

with open(sys.argv[2], 'rb') as g1:
	edgeList1 = g1.read().splitlines()
with open(sys.argv[3], 'rb') as g2:
	edgeList2 = g2.read().splitlines() 

G1 = nx.read_edgelist(edgeList1)
G2 = nx.read_edgelist(edgeList2)
   
next_num_node_1 = open(sys.argv[4],'w')
if int(round(float(x[4]))) == len(list(G1.nodes())) and int(round(float(x[9]))) == len(list(G2.nodes())):
	next_num_node_G1 = int(target_node_num)*float(x[4])/(float(x[4])+float(x[9]))
	next_num_node_1.write(str(int(round(next_num_node_G1))))
elif int(round(float(x[4]))) < 0 and int(round(float(x[9]))) > 0 :
	next_num_node_1.write(str(0))
elif int(round(float(x[4]))) > 0 and int(round(float(x[9]))) < 0 :
	next_num_node_1.write(str(int(round(float(x[4])))+int(round(float(x[9])))))
else:
	next_num_node_1.write(str(int(round(float(x[4])))))
next_num_node_1.close()
next_num_node_2 = open(sys.argv[5],'w')
if int(round(float(x[4]))) == len(list(G1.nodes())) and int(round(float(x[9]))) == len(list(G2.nodes())):
	next_num_node_G2 = int(target_node_num)*float(x[9])/(float(x[4])+float(x[9]))
	next_num_node_2.write(str(int(round(next_num_node_G2))))
elif int(round(float(x[4]))) > 0 and int(round(float(x[9]))) < 0 :
	next_num_node_2.write(str(0))
elif int(round(float(x[4]))) < 0 and int(round(float(x[9]))) > 0 :
	next_num_node_2.write(str(int(round(float(x[4])))+int(round(float(x[9])))))

else:
	next_num_node_2.write(str(int(round(float(x[9])))))
next_num_node_2.close()

