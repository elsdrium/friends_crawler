import sys

DBLP_Size = 593197
MS_Size = 1731675
crawled_node_DBLP = sys.argv[1]
crawled_node_MS = sys.argv[2]

with open('../3_SingleNetwork_DegSigma/'+sys.argv[3],'r') as f1:
    x = f1.read().split()
    
avg_deg_dblp = x[4]
var_dblp = x[5]

with open('../3_SingleNetwork_DegSigma/'+sys.argv[4],'r') as f2:
    y = f2.read().split()
    
avg_deg_ms = y[4]
var_ms = y[5]

with open('../5_Matched_DegSigma/match_avg_deg_and_var_10000_iter5.txt','r') as f3:
    z = f3.read().split()

match_num_outNode = z[6]
match_var = z[8]
episilon = 1.0

fout = open('QMSamplerInput_10000_iter5.txt','w')
fout.write(str(DBLP_Size)+' '+str(crawled_node_DBLP)+' '+str(avg_deg_dblp)+' '+str(var_dblp)+' '\
          +str(MS_Size)+' '+str(crawled_node_MS)+' '+str(avg_deg_ms)+' '+str(var_ms)+' '\
          +str(match_num_outNode)+' '+str(match_var)+' '+str(10000)+' '+str(episilon))