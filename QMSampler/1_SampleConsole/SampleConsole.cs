﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.IO;

namespace SampleCOnsole
{
    class SampleConsole
    {
        static SortedList<string,List<string>> adjList;
        static bool may_duplicate;
        static int repeat_sample;
        
        static void die(string msg)
        {
            Console.Out.WriteLine("*** Fatal Error *** : " + msg);
            Environment.Exit(0);
        }

        static void dumpGraphHashSet(string outFileName, ref HashSet<string> visited)
        {
            //List<string> sc;

            List<string> outList = visited.ToList<string>();

            int meow = 0;

            using (StreamWriter outFile = new StreamWriter(outFileName))
            {
                foreach (string vertex in outList)
                {
                    if (meow == 0)
                        outFile.Write(vertex + "\t");
                    else
                        outFile.Write(vertex + "\n");
                    meow = 1 - meow;
                }
            }
        }

        static void dumpGraph(string outFileName, ref SortedList<string,List<string>> outList)
        {
            List<string> sc;
            
            using (StreamWriter outFile = new StreamWriter(outFileName))
            {
                foreach (KeyValuePair<string,List<string>> entry in outList)
                {
                    sc = entry.Value;
                    foreach (string str in sc)
                        outFile.Write(entry.Key + "\t" + str + "\n");
                }
            }
        }

        static void loadGraph(string inFileName, out int graphSize)
        {
            adjList = new SortedList<string,List<string>>();
            string line;
            string[] tokens;
            int loaded_line = 0;
            List<string> sc;
            
            HashSet<string> distinct_nodes = new HashSet<string>();
            
            using (StreamReader inFile = new StreamReader(inFileName))
            {
                while ((line = inFile.ReadLine()) != null)
                {
                    loaded_line++;
                    tokens = line.Split(new char[]{'\t', ' ', ','}, StringSplitOptions.RemoveEmptyEntries);
                    if (tokens.Length != 2)
                        die("Error! Did not find 2 tokens splitted by proper separators.\n Input Line = " + line);
                    if (adjList.ContainsKey(tokens[0]) == false)
                    {
                        sc = new List<string>();
                        sc.Add(tokens[1]);
                        adjList.Add(tokens[0], sc);
                        distinct_nodes.Add(tokens[0]);
                        distinct_nodes.Add(tokens[1]);
                    }
                    else
                    {
                        sc = adjList[tokens[0]];

                        if (may_duplicate == true)//check duplicate entry
                        {
                            if (!sc.Contains(tokens[1])) 
                                sc.Add(tokens[1]);
                        }
                        else
                        {
                            sc.Add(tokens[1]);
                        }
                        distinct_nodes.Add(tokens[1]);
                    }
                    if (loaded_line % 500000 == 0)
                        print("[STATUS] "+loaded_line+" lines have been loaded");
                }
            }
            Console.Out.WriteLine("[INFO] |V| of input graph: " + distinct_nodes.Count);
            Console.Out.WriteLine("[INFO] total loaded lines: " + loaded_line);
            graphSize = distinct_nodes.Count;
        }

        
        static string pickStartNode()
        {
            Random rnd = new Random();
            int idx = -1;
            List<string> sc;
            string startNode;

            idx = rnd.Next(0, adjList.Count);

            //確定idx所指的人有朋友！
            startNode = adjList.Keys[idx];

            /*while(true)
            {
                idx = rnd.Next(0, adjList.Count);

                //確定idx所指的人有朋友！
                startNode = adjList.Keys[idx];
                if(adjList.TryGetValue(startNode, out sc)==false)
                {
                    Console.WriteLine("[WARNING] Cannot obtain adjacent lists of -> "+startNode);
                    continue;
                }
                if (sc.Count == 0)
                    continue;
                Console.WriteLine("[STATUS] New startNode selected: " + startNode);
                break;
            }*/
            return startNode;
        }

        static void BFS(int SampleNum, string outFileName, string outInfoFileName, int graphSize)
        {
            string curNode = pickStartNode();
            Queue<string> queue = new Queue<string>();
            List<string> sc;
            
            int cnt = 0, prev_visited_cnt = 0;
            int byebye = 0;
            HashSet<string> visited = new HashSet<string>();
            lock (queue)
            {
                queue.Enqueue(curNode);
            }
            visited.Add(curNode);

            while (true)
            {
                cnt++;

                if (cnt % 1000 == 0)
                {
                    Console.WriteLine("[STATUS] # of sampled " + visited.Count);
                    if (visited.Count == prev_visited_cnt)
                    {
                        //Console.Out.WriteLine("DEAD LOCK! JUMP!");
                        curNode = pickStartNode();
                        queue.Clear();
                        lock (queue)
                        {
                            queue.Enqueue(curNode);
                        }

                    }
                }
                if (cnt % 60000 == 0)
                {
                    //Not expecting to sample nodes anymore... too many dangling nodes
                    if (visited.Count == byebye)
                        goto Bye;
                    byebye = visited.Count;

                }

                if (queue.Count > 0)
                {
                    curNode = queue.Dequeue();
                    visited.Add(curNode);

                    //somebody may have only in-degree, 
                    //so they will act as the key of adjList
                    if (!adjList.ContainsKey(curNode))
                    {
                        curNode = pickStartNode();
                        continue;
                    }
                    sc = adjList[curNode];
                    
                    prev_visited_cnt = visited.Count();
                    foreach (string str in sc)
                    {
                        if (visited.Contains(str))
                            continue;
                        //visited.Add(str);
                        //sw.WriteLine(curNode + "\t" + str);
                        if (queue.Count() > SampleNum * 1.1)
                            break;
                        lock (queue)
                        {
                            queue.Enqueue(str);
                        }
                    }
                }
                else //queue.cont()==0
                {
                    curNode = pickStartNode();
                    lock (queue)
                    {
                        queue.Enqueue(curNode);
                    }
                }

                if (visited.Count >= SampleNum)
                {
                    Console.Out.WriteLine("Completed for sampling " + SampleNum + " nodes");
                    break;
                }
            }
        Bye:
            //StreamWriter sw_info = new StreamWriter(outInfoFileName);
            //sw_info.WriteLine("Graph_Size\t" + graphSize);
            //sw_info.WriteLine("Sampled_Nodes\t" + visited.Count);
            //sw_info.Close();
            StreamWriter sw = new StreamWriter(outFileName);
            int i = 0;
            foreach (string str in visited)
            {
                if (i == 0)
                    sw.Write(str + "\t");
                else
                    sw.WriteLine(str);
                i = 1 - i;
            }
            if (i == 1)
                sw.WriteLine("0");
            sw.Close();
        }

        static void UNIFORM(int SampleNum, string outFileName,
            string outInfoFileName, int graphSize)
        {
            List<string> sc;
            int cnt = 0, prev_visited_cnt = 0;
            int byebye = 0;
            HashSet<string> visited = new HashSet<string>();
            Random rnd = new Random();
            string curNode;
            int idx;

            while (true)
            {
                cnt++;

                while (true)
                {
                    idx = rnd.Next(0, adjList.Count);
                    if (visited.Contains(adjList.Keys[idx]))
                        continue;
                    else
                    {
                        curNode = adjList.Keys[idx];
                        break;
                    }
                }

                if (cnt % 1000 == 0)
                {
                    Console.WriteLine("[STATUS] # of sampled " + visited.Count);
                }
                if (cnt % 10000 == 0)
                {
                    //Not expecting to sample nodes anymore... too many dangling nodes
                    if (visited.Count == byebye)
                    {
                        Console.WriteLine("[WARNING] Cannot find any more. " +
                            "Current sampled = " + visited.Count);
                        break;
                    }
                    byebye = visited.Count;

                }
                visited.Add(curNode);
                prev_visited_cnt = visited.Count();
                
                if (visited.Count >= SampleNum)
                {
                    Console.Out.WriteLine("Completed for sampling " + SampleNum + " nodes");
                    break;
                }
            }
        
            
            StreamWriter sw = new StreamWriter(outFileName);
            int i=0;
            foreach (string str in visited)
            {
                if (i == 0)
                    sw.Write(str + "\t");
                else
                    sw.WriteLine(str);
                i = 1 - i;
            }
            if (i == 1)
                sw.WriteLine("0");
            sw.Close();
        }

        static void RW(int SampleNum, string outFileName,
            string outInfoFileName, int graphSize)
        {

            List<string> sc;
            int cnt = 0, prev_visited_cnt = 0;
            int byebye = 0;
            HashSet<string> visited = new HashSet<string>();
            Random rnd = new Random();
            int idx, deg_x, deg_y;
            double p;
            string y;
            //SortedList<string, List<string>> outList = new SortedList<string, List<string>>();
            string curNode = pickStartNode();

            while (true)
            {
                cnt++;

                if (cnt % 1000 == 0)
                {
                    Console.WriteLine("[STATUS] # of sampled " + visited.Count);
                    if (visited.Count == prev_visited_cnt)
                    {
                        Console.Out.WriteLine("DEAD LOCK! JUMP!");
                        curNode = pickStartNode();
                    }
                }
                if (cnt % 100000 == 0)
                {
                    //Not expecting to sample nodes anymore... too many dangling nodes
                    if (visited.Count == byebye)
                    {
                        Console.WriteLine("[WARNING] Cannot find any more. " +
                            "Current sampled = " + visited.Count);
                        goto Bye;
                    }
                    byebye = visited.Count;

                }
                visited.Add(curNode);
                prev_visited_cnt = visited.Count();
                //take a look at x's friends
                if (!adjList.ContainsKey(curNode))
                {
                    curNode = pickStartNode();
                    continue;
                }

                //randomly pick x's friend and go
                
                sc = adjList[curNode];
                idx = rnd.Next(0, sc.Count);
                y = sc.ElementAt<string>(idx);
                
                //if p<= deg_x/deg_y, y is the next step
                
                    //traverse to y, and add e(curNode,y) if not existed in outList
                    /*if (outList.ContainsKey(curNode) == false)
                    {
                        sc = new List<string>();
                        sc.Add(y);
                        outList.Add(curNode, sc);
                    }
                    else
                    {
                        sc = outList[curNode];
                        if (!sc.Contains(y))
                            sc.Add(y);
                    }*/

                    curNode = y;
                

                if (visited.Count >= SampleNum)
                {
                    Console.Out.WriteLine("Completed for sampling " + SampleNum + " nodes");
                    break;
                }
            }
        Bye:
            dumpGraphHashSet(outFileName, ref visited);
            StreamWriter sw_info = new StreamWriter(outInfoFileName);
            sw_info.WriteLine("Graph_Size\t" + graphSize);
            sw_info.WriteLine("Sampled_Nodes\t" + visited.Count);
            sw_info.Close();
            //StreamWriter sw = new StreamWriter(outFileName);
            //sw.Close();
        }
        
        static void MHRW(int SampleNum, string outFileName, 
            string outInfoFileName, int graphSize)
        {
            List<string> sc;
            int cnt = 0, prev_visited_cnt = 0;
            int byebye = 0;
            HashSet<string> visited = new HashSet<string>();
            Random rnd = new Random();
            int idx, deg_x, deg_y;
            double p;
            string y;
            //SortedList<string, List<string>> outList = new SortedList<string,List<string>>();
            string curNode = pickStartNode();

            while (true)
            {
                cnt++;

                if (cnt % 1000 == 0)
                {
                    Console.WriteLine("[STATUS] # of sampled " + visited.Count);
                    if (visited.Count == prev_visited_cnt)
                    {
                        Console.Out.WriteLine("DEAD LOCK! JUMP!");
                        curNode = pickStartNode();                      
                    }
                }
                if (cnt % 70000== 0)
                {
                    //Not expecting to sample nodes anymore... too many dangling nodes
                    if (visited.Count == byebye)
                    {
                        Console.WriteLine("[WARNING] Cannot find any more. " + 
                            "Current sampled = " + visited.Count);
                        goto Bye;
                    }
                    byebye = visited.Count;

                }
                visited.Add(curNode);
                prev_visited_cnt = visited.Count();
                //take a look at x's friends
                if (!adjList.ContainsKey(curNode))
                {
                    curNode = pickStartNode();
                    continue;
                }
                sc = adjList[curNode];
                deg_x = sc.Count;

                //select a neighbor y of x in random
                if(sc.Count==0)
                {
                    curNode = pickStartNode();
                    continue;
                }
                idx = rnd.Next(0, sc.Count);
                y = sc.ElementAt<string>(idx);

                

                //query y's degree
                if (!adjList.ContainsKey(y))
                {
                    curNode = pickStartNode();
                    continue;
                }
                deg_y = adjList[y].Count;

                //generate a random number p, uniformly between 0 and 1
                p = rnd.NextDouble();

                //if p<= deg_x/deg_y, y is the next step
                if (p <= (double)((double)deg_x / (double)deg_y))
                {
                    //Console.WriteLine(curNode+"\t"+y);
                    //traverse to y, and add e(curNode,y) if not existed in outList
                    /*if (outList.ContainsKey(curNode) == false)
                    {
                        sc = new List<string>();
                        sc.Add(y);
                        outList.Add(curNode, sc);
                    }
                    else
                    {
                        sc = outList[curNode];
                        if (!sc.Contains(y))
                            sc.Add(y);
                    }*/
                    if (visited.Contains(y) == false)
                        visited.Add(y);

                    curNode = y;
                }

                if (visited.Count >= SampleNum)
                {
                    Console.Out.WriteLine("Completed for sampling "+SampleNum+" nodes");
                    break;
                }
            }
Bye:
            //dumpGraph(outFileName, ref outList);
            StreamWriter sw = new StreamWriter(outFileName);
            int i = 0;
            foreach (string str in visited)
            {
                if (i == 0)
                    sw.Write(str + "\t");
                else
                    sw.WriteLine(str);
                i = 1 - i;
            }
            if (i == 1)
                sw.WriteLine("0");
            sw.Close();
        }

        static void printUsage()
        {
            Console.WriteLine("Inteactive mode:  MHRW -i");
            Console.Out.WriteLine("Batch mode: MHRW {--duplicate | --noduplicate} -b InputFileName OutputFileName_Prefix " +
                    "N SampleNumber_1 SamplerNumber_2 ... SampleNumber_N");
        }

        static void print(string msg)
        {
            Console.Out.WriteLine(msg);
        }

        static void itractUsage()
        {
            print("(1) {--duplicate | --noduplicate}\n'--duplicate' indicates the input graph may contain duplicated edges\n\n" +
                  "(2) repeat REP_NUM\n Let MHRW/BFS sample REP_NUM times each time\n\n" + 
                  "(2) status\nShow current status\n\n"+
                  "(3) load GraphFileName\nLoad graph into memory\n\n" +
                  "(4) sample OutFileNamePrefix SampleNumber\nSample specific number of nodes using MHRW and write to file\n\n" +
                  "(5) BFS_sample OutFileNamePrefix SampleNumber\nSample specific number of nodes using BFS and write to file\n\n" +
                  "(6) RW_sample OutFileNamePrefix SampleNumber\nSample specific number of nodes using RW and write to file\n\n" +
                  "(7) UNI_sample OutFileNamePrefix SampleNumber\nSample specific number of nodes using UNIFORM and write to file\n");
        }

        static void interactiveMode(string _loadedGraph, int _graphSize)
        {
            string cmd;
            string loadedGraph = _loadedGraph;
            string inFileName = "";
            int graphSize = _graphSize;
            string tmp;
            string[] tokens;

            repeat_sample = 1;
            while (true)
            {
                try
                {

                    Console.Write("[SampleConsole]$ ");
                    cmd = Console.ReadLine().Trim().ToLower();
                    if (cmd == "")
                        continue;
                    tokens = cmd.Split(new char[] {' ','\t'});

                    if (tokens[0] == "status")
                        if (loadedGraph != "")
                        {
                            print("may_duplicate = " + may_duplicate);
                            print("Loaded Graph: " + loadedGraph);
                            print("Repeat Sample: " + repeat_sample);
                        }
                        else
                        {
                            print("may_duplicate = " + may_duplicate);
                            print("Loaded Graph: none!");
                            print("Repeat Sample: " + repeat_sample);
                        }
                    else if (tokens[0] == "load")
                    {
                        if (tokens.Length != 2)
                        {
                            itractUsage();
                            continue;
                        }
                        inFileName = tokens[1];
                        if (File.Exists(inFileName) == false)
                        {
                            print("input file: " + inFileName + " not exists");
                            continue;
                        }
                        else
                        {
                            print("Are you sure? [y/N]");
                            tmp = Console.ReadLine().Trim().ToLower();
                            if (tmp != "y")
                            {
                                print("World Peace! No graph loaded");
                                continue;
                            }

                            print("Loading graph data " + inFileName + " ...");
                            loadGraph(inFileName, out graphSize);
                            print(inFileName + " loading completed!");
                            loadedGraph = inFileName;
                        }
                    }
                    else if (tokens[0] == "sample")
                    {
                        //sample OutFileNamePrefix SampleNumber
                        int SampleNum = 0;
                        string outFileName, outInfoFileName;
                        if (tokens.Length != 3)
                        {
                            itractUsage();
                            continue;
                        }
                        if (Int32.TryParse(tokens[2], out SampleNum) == false)
                        {
                            print("SampleNumber should be an integer");
                            continue;
                        }

                        if (graphSize == 0 || loadedGraph == "")
                        {
                            print("No graph file loaded. Please use 'load' first. ");
                            continue;
                        }

                        for (int i = 0; i < repeat_sample; i++)
                        {
                            //print("[STATUS] sampling " + (i + 1) + "/" + repeat_sample);
                            outFileName = tokens[1] + "_MHRW_" + (int)(SampleNum / 1000) + "k."+i;
                            outInfoFileName = tokens[1] + "_MHRW_" + (int)(SampleNum / 1000) + "k_info."+i;

                            print("[STATIS] MHRW Sampling! " + (i + 1) + " / " + repeat_sample + "\nOutput File Name = " + outFileName + " Info File Name =  " +
                                outInfoFileName + " Sample Number = " + SampleNum);
                            MHRW(SampleNum, outFileName, outInfoFileName, graphSize);
                        }

                    }
                    else if (tokens[0] == "exit")
                    {
                        print("YOU CANNOT PASS!");
                    }
                    else if (tokens[0] == "--duplicate")
                        may_duplicate = true;
                    else if (tokens[0] == "--noduplicate")
                        may_duplicate = false;
                    else if(tokens[0]=="repeat")
                    {
                        if (tokens.Length != 2)
                        {
                            itractUsage();
                            continue;
                        }
                        int rep;
                        if (Int32.TryParse(tokens[1], out rep) == false || rep<=0)
                        {
                            print("repeat times should be an integer >0");
                            continue;
                        }
                        repeat_sample = rep;
                    }
                    else if(tokens[0]=="bfs_sample") //use BFS
                    {
                        int SampleNum = 0;
                        string outFileName, outInfoFileName;
                        if (tokens.Length != 3)
                        {
                            itractUsage();
                            continue;
                        }
                        if (Int32.TryParse(tokens[2], out SampleNum) == false)
                        {
                            print("SampleNumber should be an integer");
                            continue;
                        }

                        if (graphSize == 0 || loadedGraph == "")
                        {
                            print("No graph file loaded. Please use 'load' first. ");
                            continue;
                        }

                        for (int i = 0; i < repeat_sample; i++)
                        {
                            //print("[STATUS] sampling " + (i + 1) + "/" + repeat_sample);
                            outFileName = tokens[1] + "_BFS_" + (int)(SampleNum / 1000) + "k." + i;
                            outInfoFileName = tokens[1] + "_BFS_" + (int)(SampleNum / 1000) + "k_info." + i;

                            print("[STATUS] BFS Sampling! " + (i + 1) + " / " + repeat_sample + "\nOutput File Name = " + outFileName + " Info File Name =  " +
                                outInfoFileName + " Sample Number = " + SampleNum);
                            BFS(SampleNum, outFileName, outInfoFileName, graphSize);
                        }
                    }
                    else if (tokens[0] == "rw_sample") //use rw
                    {
                        int SampleNum = 0;
                        string outFileName, outInfoFileName;
                        if (tokens.Length != 3)
                        {
                            itractUsage();
                            continue;
                        }
                        if (Int32.TryParse(tokens[2], out SampleNum) == false)
                        {
                            print("SampleNumber should be an integer");
                            continue;
                        }

                        if (graphSize == 0 || loadedGraph == "")
                        {
                            print("No graph file loaded. Please use 'load' first. ");
                            continue;
                        }

                        for (int i = 0; i < repeat_sample; i++)
                        {
                            //print("[STATUS] sampling " + (i + 1) + "/" + repeat_sample);
                            outFileName = tokens[1] + "_RW_" + (int)(SampleNum / 1000) + "k." + i;
                            outInfoFileName = tokens[1] + "_RW_" + (int)(SampleNum / 1000) + "k_info." + i;

                            print("[STATUS] RW Sampling! " + (i + 1) + " / " + repeat_sample + "\nOutput File Name = " + outFileName + " Info File Name =  " +
                                outInfoFileName + " Sample Number = " + SampleNum);
                            RW(SampleNum, outFileName, outInfoFileName, graphSize);
                        }
                    }
                    else if (tokens[0] == "uni_sample") //use uniform
                    {
                        int SampleNum = 0;
                        string outFileName, outInfoFileName;
                        if (tokens.Length != 3)
                        {
                            itractUsage();
                            continue;
                        }
                        if (Int32.TryParse(tokens[2], out SampleNum) == false)
                        {
                            print("SampleNumber should be an integer");
                            continue;
                        }

                        if (graphSize == 0 || loadedGraph == "")
                        {
                            print("No graph file loaded. Please use 'load' first. ");
                            continue;
                        }

                        for (int i = 0; i < repeat_sample; i++)
                        {
                            //print("[STATUS] sampling " + (i + 1) + "/" + repeat_sample);
                            outFileName = tokens[1] + "_UNI_" + (int)(SampleNum / 1000) + "k." + i;
                            outInfoFileName = tokens[1] + "_UNI_" + (int)(SampleNum / 1000) + "k_info." + i;

                            print("[STATUS] UNI Sampling! " + (i + 1) + " / " + repeat_sample + "\nOutput File Name = " + outFileName + " Info File Name =  " +
                                outInfoFileName + " Sample Number = " + SampleNum);
                            UNIFORM(SampleNum, outFileName, outInfoFileName, graphSize);
                        }
                    }
                    else
                    {
                        itractUsage();
                    }
                }
                catch (Exception e)
                {
                    print("[Exception] " + e.Message + "\n" + e.StackTrace);
                }
            }
        }

        static void Main(string[] args)
        {
            string inFileName, outFilePrefix;
            int [] SampleNum;
            int N;
            //BATCH mode
            //MHRW_Sampler {--duplicate | --noduplicate} -b InputFileName OutputFileName_Prefix N SampleNumber_1 SamplerNumber_2 ... SampleNumber_N
            //INTERATIVE mode
            //MHRW_Sampler {--duplicate | --noduplicate} -i

            //check if interative mode
            if (args.Length == 2)
            {
                if (args[1] != "-i")
                {
                    printUsage();
                    return;
                }
                if (args[0] == "--duplicate")
                    may_duplicate = true;
                else if (args[0] == "--noduplicate")
                    may_duplicate = false;
                else
                {
                    printUsage();
                    return;
                }

                interactiveMode("",0);
                
            }
            else if (args.Length >= 6  && args[1] == "-b")
            {
                if (args[0] == "--duplicate")
                    may_duplicate = true;
                else if (args[0] == "--noduplicate")
                    may_duplicate = false;
                else
                {
                    printUsage();
                    return;
                }

                inFileName = args[2];
                outFilePrefix = args[3];
                if (Int32.TryParse(args[4], out N) == false || args.Length != (5 + N))
                {
                    Console.WriteLine("N not correctly specified: " + args[4]);
                    return;
                }
                SampleNum = new int[N];
                for (int i = 0; i < N; i++)
                {
                    if (Int32.TryParse(args[5 + i], out SampleNum[i]) == false)
                    {
                        Console.WriteLine("SampleNumber_i not correctly specified: i=" + i);
                        return;
                    }

                }


                try
                {
                    int graphSize = 0;
                    loadGraph(inFileName, out graphSize);
                    Console.WriteLine("[STATUS] Graph Loading Done!");
                    foreach (int num in SampleNum)
                    {
                        if (num > graphSize)
                        {
                            Console.WriteLine("[WARNING] SampleNum = " + num + "> graph size = " + graphSize);
                            return;
                        }
                        MHRW(num, outFilePrefix + "_MHRW_" + (int)(num / 1000) + "k",
                            outFilePrefix + "_MHRW_" + (int)(num / 1000) + "k_info", graphSize);
                        //dumpGraph(outFileName);
                    }
                    //enter interactive mode

                    interactiveMode(inFileName,graphSize);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.StackTrace);
                }
            }
            else
            {
               printUsage();
                return;
            }
        }
    }
}
