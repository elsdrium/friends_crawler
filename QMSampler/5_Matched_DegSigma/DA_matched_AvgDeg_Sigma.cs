﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace DBLP_ACM_AvgDeg_Sigma
{
    class DA_matched_AvgDeg_Sigma
    {
        static void warning(string msg)
        {
            Console.WriteLine("[WARNING] " + msg);
        }
        static void Main(string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("Usage: DBLP_MS_AvgDeg_Sigma inFileName");
                return;
            }

            try
            {
                StreamReader sr = new StreamReader(args[0]);
                SortedList<int, int> inDeg = new SortedList<int, int>();
                SortedList<int, int> outDeg = new SortedList<int, int>();
                string line;
                string[] tokens;
                int first, second;
                while ((line = sr.ReadLine()) != null)
                {
                    tokens = line.Split(new char[] { ' ', ',', '\t' });
                    if (tokens.Length != 2)
                    {
                        warning("line skipped. line = " + line);
                        continue;
                    }
                    if (Int32.TryParse(tokens[0], out first) == false || Int32.TryParse(tokens[1], out second) == false)
                    {
                        warning("Unable to parse into integer. line = " + line);
                        continue;
                    }

                    if (first >= 90000000) // a matched and renamed one
                    {
                        if (outDeg.ContainsKey(first) == false)
                            outDeg.Add(first, 1);
                        else
                            outDeg[first] = outDeg[first] + 1;
                    }

                    if (second >= 90000000)
                    {
                        if (inDeg.ContainsKey(second) == false)
                            inDeg.Add(second, 1);
                        else
                            inDeg[second] = inDeg[second] + 1;
                    }

                }

                int totalOutDeg = 0, totalInDeg = 0;
                double avgOutDeg = 0.0, avgInDeg = 0.0;
                foreach(KeyValuePair<int,int> pair in outDeg)
                {
                    totalOutDeg +=pair.Value;
                }


                foreach(KeyValuePair<int,int> pair in inDeg)
                {
                    totalInDeg += pair.Value;
                }

                if (outDeg.Count == 0)
                    avgOutDeg = 0;
                else
                    avgOutDeg = (double)totalOutDeg/(double)outDeg.Count();

                if (inDeg.Count == 0)
                    avgInDeg = 0;
                else
                    avgInDeg = (double)totalInDeg/(double)inDeg.Count();

                //compute sigmaa
                double outSigma = 0.0, inSigma = 0.0;
                foreach (KeyValuePair<int, int> pair in outDeg)
                    outSigma += (pair.Value - avgOutDeg) * (pair.Value - avgOutDeg);
                outSigma /= (double)(outDeg.Count() - 1);

                foreach (KeyValuePair<int, int> pair in inDeg)
                    inSigma += (pair.Value - avgInDeg) * (pair.Value - avgInDeg);
                inSigma /= (double)(inDeg.Count() - 1);


                //Console.WriteLine("matched # outNode = " + outDeg.Count + ", matched # inNode = " + inDeg.Count);
                //Console.WriteLine("matched avg OutDeg = " + avgOutDeg + ", matched avg InDeg = " + avgInDeg);
                //Console.WriteLine("matched outSigma = " + outSigma + ", matched inSigma = " + inSigma);
                Console.WriteLine("match_num_outNode match_AvgOutDeg match_OutSigma match_num_inNode match_AvgInDeg match_InSigma");
                Console.WriteLine("{0} {1} {2} {3} {4} {5}", outDeg.Count, avgOutDeg, outSigma, inDeg.Count, avgInDeg, inSigma);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message + "\n" + e.StackTrace);
            }
        }
    }
}
