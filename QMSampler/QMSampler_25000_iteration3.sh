#!bin/bash

sampled_num_node_dblp=$(cat 6_QMSampler/num_node_dblp_25000_iter3.txt)
let rescaled_num_node_dblp=$sampled_num_node_dblp/1000
sampled_num_node_ms=$(cat 6_QMSampler/num_node_ms_25000_iter3.txt)
let rescaled_num_node_ms=$sampled_num_node_ms/1000

cd 2_Edge_Extraction 
mono reParse.exe ../MS_DBLP_Dataset/DBLP.csv iter3_dblp 1 ../1_SampleConsole/iter3_dblp_MHRW_${rescaled_num_node_dblp}k.0
python combine.py ../1_SampleConsole/iter3_dblp_MHRW_${rescaled_num_node_dblp}k.0.reparsed Merged_dblp_iter2.reparsed Merged_dblp_iter3.reparsed
mono reParse.exe ../MS_DBLP_Dataset/DBLP.csv Merged_dblp 1 Merged_dblp_iter3.reparsed

mono reParse.exe ../MS_DBLP_Dataset/MS.csv iter3_ms 1 ../1_SampleConsole/iter3_ms_MHRW_${rescaled_num_node_ms}k.0
python combine.py ../1_SampleConsole/iter3_ms_MHRW_${rescaled_num_node_ms}k.0.reparsed Merged_ms_iter2.reparsed Merged_ms_iter3.reparsed
mono reParse.exe ../MS_DBLP_Dataset/MS.csv Merged_ms 1 Merged_ms_iter3.reparsed

python check_node_num.py Merged_dblp_iter3.reparsed Merged_ms_iter3.reparsed
current_num_node_dblp=$(cat current_node_info_of_Merged_dblp_iter3.reparsed.reparsed)
current_num_node_ms=$(cat current_node_info_of_Merged_ms_iter3.reparsed.reparsed)

cd ../3_SingleNetwork_DegSigma
mono SingleNetwork_avgDeg_Sigma.exe ../2_Edge_Extraction/Merged_dblp_iter3.reparsed.reparsed > avg_deg_and_var_of_DBLP_${current_num_node_dblp}_iter3.txt
mono SingleNetwork_avgDeg_Sigma.exe ../2_Edge_Extraction/Merged_ms_iter3.reparsed.reparsed > avg_deg_and_var_of_MS_${current_num_node_ms}_iter3.txt

cd ../4_MatchMerge
mono MatchMerge.exe ../MS_DBLP_Dataset/matchList.dat ../2_Edge_Extraction/Merged_dblp_iter3.reparsed.reparsed ../2_Edge_Extraction/Merged_ms_iter3.reparsed.reparsed 90000000 Merged_25000_iter3.txt

cd ../5_Matched_DegSigma
mono DA_matched_AvgDeg_Sigma.exe ../4_MatchMerge/Merged_25000_iter3.txt > match_avg_deg_and_var_25000_iter3.txt

let current_sum_nodes=$current_num_node_dblp+$current_num_node_ms
if [ "$current_sum_nodes" -lt 100000 ]; then
	cd ../6_QMSampler
	python QMSamplerInput_25000_iter3.py ${current_num_node_dblp} ${current_num_node_ms} avg_deg_and_var_of_DBLP_${current_num_node_dblp}_iter3.txt avg_deg_and_var_of_MS_${current_num_node_ms}_iter3.txt
	./QMSampler < QMSamplerInput_25000_iter3.txt
	python next_num_node_and_edge.py 25000 ../2_Edge_Extraction/Merged_dblp_iter3.reparsed.reparsed ../2_Edge_Extraction/Merged_ms_iter3.reparsed.reparsed num_node_dblp_25000_iter4.txt num_node_ms_25000_iter4.txt
else
	echo "End. Please check the merged file."
fi
