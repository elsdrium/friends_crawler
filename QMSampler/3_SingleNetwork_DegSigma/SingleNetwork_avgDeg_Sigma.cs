﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace AvgDeg_Sigma
{
    class AvgDeg_Sigma
    {
        static void status(string msg)
        {
            Console.WriteLine("[STATUS] " + msg);
        }

        static void warning(string msg)
        {
            Console.WriteLine("[WARNING] " + msg);
        }

        static void Main(string[] args)
        {
            if(args.Length!=1)
                Console.WriteLine("Usage: AvgDeg_Sigma inFileName");

            try
            {
                StreamReader sr = new StreamReader(args[0]);
                SortedList<int, int> outDeg = new SortedList<int, int>();
                SortedList<int, int> inDeg = new SortedList<int, int>();
                string line;
                string[] tokens;
                int first_node, second_node;
                int tmp;
                int TotalInDeg = 0, TotalOutDeg = 0;

                while ((line = sr.ReadLine()) != null)
                {
                    tokens = line.Split(new char[] { ' ', '\t', ',' }, StringSplitOptions.RemoveEmptyEntries);
                    if (tokens.Length != 2)
                    {
                        warning("Line skipped. line = " + line);
                        continue;
                    }

                    if (Int32.TryParse(tokens[0], out first_node) == false || Int32.TryParse(tokens[1], out second_node) == false)
                    {
                        warning("Not a integer id. line = " + line);
                        continue;
                    }

                    //pass 0: shit... there may be duplicated edge (pair of nodes)

                    //pass 1: calculate avg. degree
                    if (!outDeg.ContainsKey(first_node))
                    {
                        TotalOutDeg++;
                        outDeg.Add(first_node, 1);
                    }
                    else
                    {
                        TotalOutDeg++;
                        outDeg[first_node] = outDeg[first_node] + 1;
                    }

                    if (!inDeg.ContainsKey(second_node))
                    {
                        TotalInDeg++;
                        inDeg.Add(second_node, 1);
                    }
                    else
                    {
                        TotalInDeg++;
                        inDeg[second_node] = inDeg[second_node] + 1;
                    }
                }
                sr.Close();

                double AvgOutDeg = (double)TotalOutDeg / (double)(Math.Max(outDeg.Count(),inDeg.Count()));
                double AvgInDeg = (double)TotalInDeg / (double)(Math.Max(outDeg.Count(),inDeg.Count()));

                //pass 2, compute sigmamama
                double outSigma = 0.0, inSigma = 0.0;
                foreach (KeyValuePair<int, int> pair in outDeg)
                    outSigma += (pair.Value - AvgOutDeg) * (pair.Value - AvgOutDeg);
                outSigma /= (double)(outDeg.Count() - 1);

                foreach (KeyValuePair<int, int> pair in inDeg)
                    inSigma += (pair.Value - AvgInDeg) * (pair.Value - AvgInDeg);
                inSigma /= (double)(inDeg.Count() - 1);


                Console.WriteLine("AvgOutDeg OutSigma AvgInDeg InSigma");
                Console.WriteLine(AvgOutDeg+" "+outSigma+" "+" "+AvgInDeg+" "+inSigma);
                //Console.WriteLine("Avg In Deg , Avg Out Deg = " + AvgInDeg + " , " + AvgOutDeg);
                //Console.WriteLine("In Sigma , Out Sigma = " + inSigma + " , " + outSigma);
                //Console.Read();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message + "\n" + e.StackTrace);
            }
            

        }
    }
}
