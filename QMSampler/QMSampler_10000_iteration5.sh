#!bin/bash

sampled_num_node_dblp=$(cat 6_QMSampler/num_node_dblp_10000_iter5.txt)
let rescaled_num_node_dblp=$sampled_num_node_dblp/1000
sampled_num_node_ms=$(cat 6_QMSampler/num_node_ms_10000_iter5.txt)
let rescaled_num_node_ms=$sampled_num_node_ms/1000

cd 2_Edge_Extraction 
mono reParse.exe ../MS_DBLP_Dataset/DBLP.csv iter5_dblp 1 ../1_SampleConsole/iter5_dblp_MHRW_${rescaled_num_node_dblp}k.0
python combine.py ../1_SampleConsole/iter5_dblp_MHRW_${rescaled_num_node_dblp}k.0.reparsed Merged_dblp_iter4.reparsed Merged_dblp_iter5.reparsed
mono reParse.exe ../MS_DBLP_Dataset/DBLP.csv Merged_dblp 1 Merged_dblp_iter5.reparsed

mono reParse.exe ../MS_DBLP_Dataset/MS.csv iter5_ms 1 ../1_SampleConsole/iter5_ms_MHRW_${rescaled_num_node_ms}k.0
python combine.py ../1_SampleConsole/iter5_ms_MHRW_${rescaled_num_node_ms}k.0.reparsed Merged_ms_iter4.reparsed Merged_ms_iter5.reparsed
mono reParse.exe ../MS_DBLP_Dataset/MS.csv Merged_ms 1 Merged_ms_iter5.reparsed

python check_node_num.py Merged_dblp_iter5.reparsed Merged_ms_iter5.reparsed
current_num_node_dblp=$(cat current_node_info_of_Merged_dblp_iter5.reparsed.reparsed)
current_num_node_ms=$(cat current_node_info_of_Merged_ms_iter5.reparsed.reparsed)

cd ../3_SingleNetwork_DegSigma
mono SingleNetwork_avgDeg_Sigma.exe ../2_Edge_Extraction/Merged_dblp_iter5.reparsed.reparsed > avg_deg_and_var_of_DBLP_${current_num_node_dblp}_iter5.txt
mono SingleNetwork_avgDeg_Sigma.exe ../2_Edge_Extraction/Merged_ms_iter5.reparsed.reparsed > avg_deg_and_var_of_MS_${current_num_node_ms}_iter5.txt

cd ../4_MatchMerge
mono MatchMerge.exe ../MS_DBLP_Dataset/matchList.dat ../2_Edge_Extraction/Merged_dblp_iter5.reparsed.reparsed ../2_Edge_Extraction/Merged_ms_iter5.reparsed.reparsed 90000000 Merged_10000_iter5.txt

cd ../5_Matched_DegSigma
mono DA_matched_AvgDeg_Sigma.exe ../4_MatchMerge/Merged_10000_iter5.txt > match_avg_deg_and_var_10000_iter5.txt

cd ../6_QMSampler
python QMSamplerInput_10000_iter5.py ${current_num_node_dblp} ${current_num_node_ms} avg_deg_and_var_of_DBLP_${current_num_node_dblp}_iter5.txt avg_deg_and_var_of_MS_${current_num_node_ms}_iter5.txt
./QMSampler < QMSamplerInput_10000_iter5.txt
python next_num_node_and_edge.py 10000 ../2_Edge_Extraction/Merged_dblp_iter5.reparsed.reparsed ../2_Edge_Extraction/Merged_ms_iter5.reparsed.reparsed num_node_dblp_10000_iter6.txt num_node_ms_10000_iter6.txt
