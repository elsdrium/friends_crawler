import sys
import networkx as nx

with open('GroundTruth.out','rb') as GT:
    edgeListGT = GT.read().splitlines()

with open('4_MatchMerge/Merged_50000_iter2.txt','rb') as G50000:
    edgeListG50000 = G50000.read().splitlines()
    
with open('4_MatchMerge/Merged_25000_iter3.txt','rb') as G25000:
    edgeListG25000 = G25000.read().splitlines()
    
with open('4_MatchMerge/Merged_20000_iter4.txt','rb') as G20000:
    edgeListG20000 = G20000.read().splitlines()
    
with open('4_MatchMerge/Merged_10000_iter9.txt','rb') as G10000:
    edgeListG10000 = G10000.read().splitlines()
    
Graph_GT = nx.read_edgelist(edgeListGT)
Graph_50000 = nx.read_edgelist(edgeListG50000)
Graph_25000 = nx.read_edgelist(edgeListG25000)
Graph_20000 = nx.read_edgelist(edgeListG20000)
Graph_10000 = nx.read_edgelist(edgeListG10000)

CC_GT = nx.clustering(Graph_GT)
Avg_CC_GT = sum(CC_GT.values()) / len(CC_GT)
CC_50000 = nx.clustering(Graph_50000)
Avg_CC_50000 = sum(CC_50000.values()) / len(CC_50000)
CC_25000 = nx.clustering(Graph_25000)
Avg_CC_25000 = sum(CC_25000.values()) / len(CC_25000)
CC_20000 = nx.clustering(Graph_20000)
Avg_CC_20000 = sum(CC_20000.values()) / len(CC_20000)
CC_10000 = nx.clustering(Graph_10000)
Avg_CC_10000 = sum(CC_10000.values()) / len(CC_10000)

dseq_GT = sorted(nx.degree(Graph_GT).values(),reverse=True)
print "Degree sequence of ground truth: ", dseq_GT
dseq_50000 = sorted(nx.degree(Graph_50000).values(),reverse=True)
print "Degree sequence of the result by sampling 50000 nodes at beginning: ", dseq_50000
dseq_25000 = sorted(nx.degree(Graph_25000).values(),reverse=True)
print "Degree sequence of the result by sampling 25000 nodes at beginning: ", dseq_25000
dseq_20000 = sorted(nx.degree(Graph_20000).values(),reverse=True)
print "Degree sequence of the result by sampling 20000 nodes at beginning: ", dseq_20000
dseq_10000 = sorted(nx.degree(Graph_10000).values(),reverse=True)
print "Degree sequence of the result by sampling 10000 nodes at beginning: ", dseq_10000