import sys
import networkx as nx
import collections

with open('GroundTruth.out','rb') as GT:
    edgeListGT = GT.read().splitlines()
print "File reading done."
with open('4_MatchMerge/Merged_50000_iter2.txt','rb') as G50000:
    edgeListG50000 = G50000.read().splitlines()
print "File reading done."
with open('4_MatchMerge/Merged_25000_iter3.txt','rb') as G25000:
    edgeListG25000 = G25000.read().splitlines()
print "File reading done."
with open('4_MatchMerge/Merged_20000_iter4.txt','rb') as G20000:
    edgeListG20000 = G20000.read().splitlines()
print "File reading done."
with open('4_MatchMerge/Merged_10000_iter9.txt','rb') as G10000:
    edgeListG10000 = G10000.read().splitlines()
print "File reading done."
    
Graph_GT = nx.read_edgelist(edgeListGT)
print "Edge list reading done."
Graph_50000 = nx.read_edgelist(edgeListG50000)
print "Edge list reading done."
Graph_25000 = nx.read_edgelist(edgeListG25000)
print "Edge list reading done."
Graph_20000 = nx.read_edgelist(edgeListG20000)
print "Edge list reading done."
Graph_10000 = nx.read_edgelist(edgeListG10000)
print "Edge list reading done."

# CC_GT = nx.clustering(Graph_GT)
# Avg_CC_GT = sum(CC_GT.values()) / len(CC_GT)
# print "Average clustering coefficient of the ground truth: ", Avg_CC_GT
# CC_50000 = nx.clustering(Graph_50000)
# Avg_CC_50000 = sum(CC_50000.values()) / len(CC_50000)
# print "Average clustering coefficient of the result by sampling 50000 nodes at beginning: ", Avg_CC_50000
# CC_25000 = nx.clustering(Graph_25000)
# Avg_CC_25000 = sum(CC_25000.values()) / len(CC_25000)
# print "Average clustering coefficient of the result by sampling 25000 nodes at beginning: ", Avg_CC_25000
# CC_20000 = nx.clustering(Graph_20000)
# Avg_CC_20000 = sum(CC_20000.values()) / len(CC_20000)
# print "Average clustering coefficient of the result by sampling 20000 nodes at beginning: ", Avg_CC_20000
# CC_10000 = nx.clustering(Graph_10000)
# Avg_CC_10000 = sum(CC_10000.values()) / len(CC_10000)
# print "Average clustering coefficient of the result by sampling 10000 nodes at beginning: ", Avg_CC_10000

dseq_GT = sorted(nx.degree(Graph_GT).values())
#print "Degree sequence of ground truth: ", dseq_GT
dist_GT = collections.Counter(dseq_GT)
print(dist_GT.keys(), dist_GT.values())
dseq_50000 = sorted(nx.degree(Graph_50000).values())
#print "Degree sequence of the result by sampling 50000 nodes at beginning: ", dseq_50000
dist_50000 = collections.Counter(dseq_50000)
print(dist_50000.keys(), dist_50000.values())
dseq_25000 = sorted(nx.degree(Graph_25000).values())
#print "Degree sequence of the result by sampling 25000 nodes at beginning: ", dseq_25000
dist_25000 = collections.Counter(dseq_25000)
print(dist_25000.keys(), dist_25000.values())
dseq_20000 = sorted(nx.degree(Graph_20000).values())
#print "Degree sequence of the result by sampling 20000 nodes at beginning: ", dseq_20000
dist_20000 = collections.Counter(dseq_20000)
print(dist_20000.keys(), dist_20000.values())
dseq_10000 = sorted(nx.degree(Graph_10000).values())
#print "Degree sequence of the result by sampling 10000 nodes at beginning: ", dseq_10000
dist_10000 = collections.Counter(dseq_10000)
print(dist_10000.keys(), dist_10000.values())

#fout = open('deg_similarity.txt', 'w')
#fout.write()
#fout.close()