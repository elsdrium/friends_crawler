﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;


namespace TriMerge
{
    class TriMerger
    {

        static void TriMerge(string flickr_File, string twitter_File, string foursq_File, string outMergedFile,
            Dictionary<int, int> dict_flickr_rename, Dictionary<int, int> dict_twitter_rename, Dictionary<int, int> dict_4sq_rename)
        {
            string line;
            int line_cnt = 0;
            int skipped_line = 0;
            int tmp1, tmp2;
            string[] tokens;

            int twitter_RenameBase = 100000000;
            int foursq_RenameBase = 200000000;
            int lines_modified = 0;

            StreamWriter sw = new StreamWriter(outMergedFile);

            // processing flickr graph
            StreamReader sr = new StreamReader(flickr_File);

            while ((line = sr.ReadLine()) != null)
            {
                line_cnt++;

                if (line_cnt % 100000 == 0)
                    Console.WriteLine(flickr_File + ":  Number of lines read: " + line_cnt);
                tokens = line.Split(new char[] { ' ', ',', '\t', '|' }, StringSplitOptions.RemoveEmptyEntries);
                if (tokens.Length != 2)
                {
                    Console.WriteLine("[Warning] line skipped!");
                    skipped_line++;
                }


                if (Int32.TryParse(tokens[0], out tmp1) && Int32.TryParse(tokens[1], out tmp2))
                {
                    bool matched = false;
                    if (dict_flickr_rename.ContainsKey(tmp1))
                    {
                        tmp1 = dict_flickr_rename[tmp1];
                        matched = true;
                    }
                    if (dict_flickr_rename.ContainsKey(tmp2))
                    {
                        tmp2 = dict_flickr_rename[tmp2];
                        matched = true;
                    }

                    sw.WriteLine(tmp1 + " " + tmp2);

                    if (matched)
                        lines_modified++;
                }
            }

            sr.Close();
            line_cnt = 0;

            // processing twitter graph
            sr = new StreamReader(twitter_File);

            while ((line = sr.ReadLine()) != null)
            {
                line_cnt++;

                if (line_cnt % 100000 == 0)
                    Console.WriteLine(twitter_File + ":  Number of lines read: " + line_cnt);
                tokens = line.Split(new char[] { ' ', ',', '\t', '|' }, StringSplitOptions.RemoveEmptyEntries);
                if (tokens.Length != 2)
                {
                    Console.WriteLine("[Warning] line skipped!");
                    skipped_line++;
                }


                if (Int32.TryParse(tokens[0], out tmp1) && Int32.TryParse(tokens[1], out tmp2))
                {
                    bool matched = false;
                    if (dict_twitter_rename.ContainsKey(tmp1))
                    {
                        tmp1 = dict_twitter_rename[tmp1];
                        matched = true;
                    }
                    else
                        tmp1 += twitter_RenameBase;


                    if (dict_twitter_rename.ContainsKey(tmp2))
                    {
                        tmp2 = dict_twitter_rename[tmp2];
                        matched = true;
                    }
                    else
                        tmp2 += twitter_RenameBase;

                    sw.WriteLine(tmp1 + " " + tmp2);
                    if (matched)
                        lines_modified++;
                }
            }

            sr.Close();
            line_cnt = 0;


            // processing 4sq graph
            sr = new StreamReader(foursq_File);

            while ((line = sr.ReadLine()) != null)
            {
                line_cnt++;

                if (line_cnt % 100000 == 0)
                    Console.WriteLine(foursq_File + ":  Number of lines read: " + line_cnt);
                tokens = line.Split(new char[] { ' ', ',', '\t', '|' }, StringSplitOptions.RemoveEmptyEntries);
                if (tokens.Length != 2)
                {
                    Console.WriteLine("[Warning] line skipped!");
                    skipped_line++;
                }


                if (Int32.TryParse(tokens[0], out tmp1) && Int32.TryParse(tokens[1], out tmp2))
                {
                    bool matched = false;
                    if (dict_4sq_rename.ContainsKey(tmp1))
                    {
                        tmp1 = dict_4sq_rename[tmp1];
                        matched = true;
                    }
                    else
                        tmp1 += foursq_RenameBase;

                    if (dict_4sq_rename.ContainsKey(tmp2))
                    {
                        tmp2 = dict_4sq_rename[tmp2];
                        matched = true;
                    }
                    else
                        tmp2 += foursq_RenameBase;

                    sw.WriteLine(tmp1 + " " + tmp2);
                    if (matched)
                        lines_modified++;
                }
            }

            sr.Close();
            sw.Close();

            Console.WriteLine(lines_modified + " lines effected!");
        }

        static void renameNodes(Dictionary<int,int> dict_flickr_twitter, Dictionary<int,int> dict_flickr_4sq, Dictionary<int,int> dict_twitter_4sq,
            Dictionary<int, int> dict_flickr_rename, Dictionary<int, int> dict_twitter_rename, Dictionary<int, int> dict_4sq_rename)
        {
            
            int flickrID, twitterID, foursqID;
            int renameID = 300000000;

            //1) processing flickr_twitter
            for (int i = 0; i < dict_flickr_twitter.Count(); i++)
            {
                flickrID = dict_flickr_twitter.ElementAt(i).Key;
                twitterID = dict_flickr_twitter.ElementAt(i).Value;

                if (dict_flickr_rename.ContainsKey(flickrID) || dict_twitter_rename.ContainsKey(twitterID))
                    continue;

                //fill into dict_flickr_rename
                dict_flickr_rename[flickrID] = renameID;
                dict_twitter_rename[twitterID] = renameID;

                //see if the flickrID has also matched to a 4sq ID
                if (dict_flickr_4sq.ContainsKey(flickrID))
                {
                    foursqID = dict_flickr_4sq[flickrID];
                    if (dict_4sq_rename.ContainsKey(foursqID))
                        continue;
                    dict_4sq_rename[foursqID] = renameID;
                }

                renameID++;
            }

            //2) processing flickr_4sq
            for (int i = 0; i < dict_flickr_4sq.Count(); i++)
            {
                flickrID = dict_flickr_4sq.ElementAt(i).Key;
                foursqID = dict_flickr_4sq.ElementAt(i).Value;

                if (dict_flickr_rename.ContainsKey(flickrID) || dict_4sq_rename.ContainsKey(foursqID))
                    continue;

                //fill into dict_flickr_rename
                dict_flickr_rename[flickrID] = renameID;
                dict_4sq_rename[foursqID] = renameID;

                //see if the flickrID has also matched to a twitter ID
                if (dict_flickr_twitter.ContainsKey(flickrID))
                {
                    twitterID = dict_flickr_twitter[flickrID];
                    if (dict_twitter_rename.ContainsKey(twitterID))
                        continue;
                    dict_twitter_rename[twitterID] = renameID;
                }

                renameID++;
            }

            //3) processing twitter_4sq
            for (int i = 0; i < dict_twitter_4sq.Count(); i++)
            {
                twitterID = dict_twitter_4sq.ElementAt(i).Key;
                foursqID = dict_twitter_4sq.ElementAt(i).Value;

                if (dict_twitter_rename.ContainsKey(twitterID) || dict_4sq_rename.ContainsKey(foursqID))
                    continue;

                //fill into dict_flickr_rename
                dict_twitter_rename[twitterID] = renameID;
                dict_4sq_rename[foursqID] = renameID;

                //see if the twitterID has also matched to a flickr ID
                /*if (dict_flickr_twitter.ContainsValue(twitterID))
                {
                    flickrID = dict_flickr_twitter.v
                    if (dict_flickr_rename.ContainsKey(flickrID))
                        continue;
                    dict_flickr_rename[flickrID] = renameID;
                }*/

                renameID++;
            }


        }


        static void readMatchFile(string matchFile, Dictionary<int,int> dict)
        {
            string line;
            int line_cnt = 0;
            int skipped_line = 0;
            int tmp1, tmp2;
            string [] tokens;
            StreamReader sr = new StreamReader(matchFile);

            while ((line = sr.ReadLine()) != null)
            {
                line_cnt++;

                //if (line_cnt % 100000 == 0)
                //    Console.WriteLine("Number of lines read: " + line_cnt);
                tokens = line.Split(new char[] { ' ', ',', '\t','|' }, StringSplitOptions.RemoveEmptyEntries);
                if (tokens.Length != 2)
                {
                    Console.WriteLine("[Warning] line skipped!");
                    skipped_line++;
                }


                if (Int32.TryParse(tokens[0], out tmp1) && Int32.TryParse(tokens[1], out tmp2))
                {
                    if(dict.ContainsKey(tmp1)==false)
                    {
                        dict.Add(tmp1, tmp2);
                    }
                }
            }

            Console.WriteLine("Match File: "+ matchFile +" loaded completely. " + line_cnt + " lines loaded");
        }

        static void usage()
        {
            Console.WriteLine("TriMerge Flickr_Graph Twitter_Graph 4sq_Graph Flickr_Twitter_MatchFile Flickr_4sq_MatchFile Twitter_4sq_MatchFile OutputMergedFileName");
        }

        static void Main(string[] args)
        {
            string flickr_twitter_MatchFile = "";
            string flickr_4sq_MatchFile = "";
            string twitter_4sq_MatchFile = "";

            string flickr_File;
            string twitter_File;
            string foursq_File;
            string outMergedFile;


            Dictionary <int,int> dict_flickr_twitter = new Dictionary<int,int>();
            Dictionary<int, int> dict_flickr_4sq = new Dictionary<int, int>();
            Dictionary<int, int> dict_twitter_4sq = new Dictionary<int, int>();

            //<flickr_node_id, new_assigned_id>
            Dictionary<int, int> dict_flickr_rename = new Dictionary<int, int>();

            //<twitter_node_id, new_assigned_id>
            Dictionary<int, int> dict_twitter_rename = new Dictionary<int, int>();

            //<4sq_node_id, new_assigned_id>
            Dictionary<int, int> dict_4sq_rename = new Dictionary<int, int>();


            if (args.Length != 7)
            {
                usage();
                return;
            }

            flickr_File = args[0];
            twitter_File = args[1];
            foursq_File = args[2];

            flickr_twitter_MatchFile = args[3];
            flickr_4sq_MatchFile = args[4];
            twitter_4sq_MatchFile = args[5];
            outMergedFile = args[6];

            readMatchFile(flickr_twitter_MatchFile, dict_flickr_twitter);
            readMatchFile(flickr_4sq_MatchFile, dict_flickr_4sq);
            readMatchFile(twitter_4sq_MatchFile, dict_twitter_4sq);

            renameNodes(dict_flickr_twitter, dict_flickr_4sq, dict_twitter_4sq, dict_flickr_rename, dict_twitter_rename, dict_4sq_rename);

            TriMerge(flickr_File, twitter_File, foursq_File, outMergedFile, dict_flickr_rename, dict_twitter_rename, dict_4sq_rename);

            //Console.WriteLine("Merge completed!\nPress Any Key to Continue...");
            //Console.Read();
        }
    }
}
