﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;

namespace MatchMerger
{
    class MatchMerge
    {
        static void die(string msg)
        {
            Console.WriteLine("[ERROR]: " + msg + "\nProgram is terminated");
            Environment.Exit(0);
        }

        static void warning(string msg)
        {
            Console.WriteLine("[WARNING]: " + msg);
        }

        static void status(string msg)
        {
            Console.WriteLine("[STATUS]: " + msg);
        }

        //static void DFS_DBLP_ACM(ref SortedList<string, List<string>> matchGraph, out SortedList<string, string> lookupTable, string renameBase)
        //{
        //}

        static void DFS_DBLP_ACM(ref SortedList<string, List<string>> matchGraph, out SortedList<string, int>lookupTable, int renameBase)
        {
            Stack<string> stack = new Stack<string>();
            HashSet<string> visited = new HashSet<string>();
            lookupTable = new SortedList<string, int>();

            //dfs
            string v;
            List<string> neighbor;
            int CC_cnt = 0;
            bool getNewStartNode = false;
            while (true)
            {
                //status("In DFS_DBLP_ACM: total node counts in matchGraph: " + matchGraph.Count() 
                //    + "\n visited node counts = "+visited.Count());

                getNewStartNode = false;
                //init
                for(int i=0;i<matchGraph.Count();i++)
                {
                    if(!visited.Contains(matchGraph.Keys[i]))
                    {
                        stack.Push(matchGraph.Keys[i]);
                        getNewStartNode = true;
                        CC_cnt++;
                        break;
                    }
                }
                
                if(getNewStartNode == false) //terminate the dfs
                    break;

                while (stack.Count() > 0)
                {
                    v = stack.Pop();
                    if (!visited.Contains(v))
                    {
                        visited.Add(v);
                        neighbor = matchGraph[v];
                        foreach (string meow in neighbor)
                            stack.Push(meow);

                        lookupTable.Add(v, renameBase + CC_cnt);
                    }
                }
            }
            status("LookupTable built completed! Total " + CC_cnt + " groups out of " + visited.Count() + " IDs have been identified");

        }


        static void Merge(ref SortedList<string, int> lookupTable, string G1FileName, string G2FileName, string outFileName)
        {
            StreamReader sr;
            string line;
            string[] tokens;
            StreamWriter sw = new StreamWriter(outFileName);
            int wroteLines = 0;
            
            //G1FileName
            sr = new StreamReader(G1FileName);
            while ((line = sr.ReadLine()) != null)
            {
                tokens = line.Split(new char[] { ' ', ',', '\t' });
                if (tokens.Length != 2)
                {
                    warning("Parsing Exception! not a pair in the line! line = " + line);
                    continue;
                }

                //for tokens[0]
                if (lookupTable.ContainsKey("f_"+tokens[0]))
                    sw.Write(lookupTable["f_" + tokens[0]]);
                else
                    sw.Write(tokens[0]);
                sw.Write("\t");

                //for tokens[1]
                if (lookupTable.ContainsKey("f_"+tokens[1]))
                    sw.Write(lookupTable["f_"+tokens[1]]);
                else
                    sw.Write(tokens[1]);
                sw.WriteLine("");
                wroteLines++;

            }
            sr.Close();

            //G2FileNames
            sr = new StreamReader(G2FileName);
            while ((line = sr.ReadLine()) != null)
            {
                tokens = line.Split(new char[] { ' ', ',', '\t' });
                if (tokens.Length != 2)
                {
                    warning("Parsing Exception! not a pair in the line! line = " + line);
                    continue;
                }

                //for tokens[0]
                if (lookupTable.ContainsKey("s_"+tokens[0]))
                    sw.Write(lookupTable["s_"+tokens[0]]);
                else
                    sw.Write(tokens[0]);
                sw.Write("\t");

                //for tokens[1]
                if (lookupTable.ContainsKey("s_"+tokens[1]))
                    sw.Write(lookupTable["s_" + tokens[1]]);
                else
                    sw.Write(tokens[1]);
                sw.WriteLine("");
                wroteLines++;

            }
            sr.Close();
            sw.Close();
            status("Merge ( " + G1FileName + " , " + G2FileName + " ) ==> " + outFileName + " completed! "+wroteLines+ " lines wroted.");
        }

        static void readMatchFile(string fileName, out SortedList<string, List<string>> matchGraph)
        {
            matchGraph = new SortedList<string, List<string>>();

            StreamReader sr = new StreamReader(fileName);
            string line;
            string[] tokens;
            int loadedLines = 0;
            List<string> neighbor;
            while ((line = sr.ReadLine()) != null)
            {
                tokens = line.Split(new char[]{' ',',','\t'});
                if (tokens.Length != 2)
                    die("parse error, line = "+line);
               
                //rename first id
                tokens[0] = "f_" + tokens[0];

                //rename second id
                tokens[1] = "s_" + tokens[1];

                //add this edge to matchGraph (forward direction)
                if (!matchGraph.ContainsKey(tokens[0]))
                {
                    neighbor = new List<string>();
                    neighbor.Add(tokens[1]);
                    matchGraph.Add(tokens[0], neighbor);
                }
                else
                {
                    neighbor = matchGraph[tokens[0]];
                    neighbor.Add(tokens[1]);
                }

                //add this edge to matchGraph (backward direction)
                if (!matchGraph.ContainsKey(tokens[1]))
                {
                    neighbor = new List<string>();
                    neighbor.Add(tokens[0]);
                    matchGraph.Add(tokens[1], neighbor);
                }
                else
                {
                    neighbor = matchGraph[tokens[1]];
                    neighbor.Add(tokens[0]);
                }
               
                loadedLines++;
            }
            status("MatchTable created! Total " + loadedLines + " imported.");
            sr.Close();
        }
        static void Main(string[] args)
        {
            string MatchListFileName, G1FileName, G2FileName, renameBase, outFileName;
            SortedList<string, List<string>> matchGraph;
            //args MatchMerger MatchListFileName Graph_1_FileName Graph_2_FileName
            if (args.Length != 5)
            {
                Console.WriteLine("Usage: MatchMerger MatchListFileName Graph_1_FileName Graph_2_FileName renameBase outFileName");
            }
            
            try
            {
                MatchListFileName = args[0];
                G1FileName = args[1];
                G2FileName = args[2];
                renameBase = args[3];
                outFileName = args[4];

                status("MatchListFileName = " + MatchListFileName + "\nGraph_1_FileName = " 
                    + G1FileName + "\nGraph_2_FileName = " + G2FileName +"\nrenameBase = "+renameBase + "\noutFileName "+ outFileName);

            
                SortedList<string, int> lookupTable;
                int rnB;
                if (Int32.TryParse(renameBase, out rnB) == false)
                    die("renameBase should be integer for DBLP and ACM matching");
                readMatchFile(MatchListFileName, out matchGraph);
                DFS_DBLP_ACM(ref matchGraph, out lookupTable, rnB);
                Merge(ref lookupTable, G1FileName, G2FileName, outFileName);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message + "\n" + e.StackTrace);
            }

        }
    }
}
