#!/bin/sh

python3 crawler.py -t foursquare -n $n
python3 crawler.py -t flickr -n $n

cd QMSampler/3_SingleNetwork_DegSigma
mono SingleNetwork_avgDeg_Sigma.exe ../../foursquare/edge_list > avg_deg_and_var_of_foursquare
mono SingleNetwork_avgDeg_Sigma.exe ../../flickr/edge_list.out > avg_deg_and_var_of_flickr
cd ../4_MatchMerge
mono MatchMerge.exe ../../matchList.dat ../3_SingleNetwork_DegSigma/foursquare/edge_list ../3_SingleNetwork_DegSigma/flickr/edge_list.out 90000000 Merged_edge_list
cd ../5_Matched_DegSigma
mono DA_matched_AvgDeg_Sigma.exe ../4_MatchMerge/Merged_edge_list > match_avg_deg_and_var_edge_list
cd ../6_QMSampler
./QMSampler < QMSamplerInput_10000_iter0.txt
