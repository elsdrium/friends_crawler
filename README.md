# Quick Start
`pip install -r requirements.txt`

`n=10000 target=5000 eps=1 flickrsize=5500000 foursquaresize=6600000 ./demo_flow.sh`

# Usage Details
`python3 crawler.py -h`

# Requirements
 - ### [mono](http://www.mono-project.com/)
 - ### Python 3

