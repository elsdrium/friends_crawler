#!/bin/sh

let iter=$n/$target

echo "Start crawling FourSquare."
python3 crawler.py -t foursquare -n $((target/2))
echo "The edge list of FourSquare is done."

echo "Start crawling Flickr."
python3 crawler.py -t flickr -n $((target/2))
echo "The edge list of Flickr is done."

echo "Now compress the IDs in the edge list of Flickr."
python3 compress_id.py flickr/edge_list
echo "Finish compressing. The compressed file is edge_list.out"

cp foursquare/edge_list ./Merged_foursquare_iter0
cp flickr/edge_list.out ./Merged_flickr_iter0

python3 check_node_num.py Merged_foursquare_iter0 Merged_flickr_iter0
current_num_node_foursquare=$(cat current_node_info_of_Merged_foursquare_iter0)
current_num_node_flickr=$(cat current_node_info_of_Merged_flickr_iter0)

cd QMSampler/3_SingleNetwork_DegSigma
echo "Calculating the average degree and variance of the graph crawled from FourSquare."
mono SingleNetwork_avgDeg_Sigma.exe ../../foursquare/edge_list > avg_deg_and_var_of_foursquare
cat avg_deg_and_var_of_foursquare

echo "Calculating the average degree and variance of the graph crawled from Flickr."
mono SingleNetwork_avgDeg_Sigma.exe ../../flickr/edge_list.out > avg_deg_and_var_of_flickr
cat avg_deg_and_var_of_flickr

cd ../4_MatchMerge
mono MatchMerge.exe ../../match_list ../../foursquare/edge_list ../../flickr/edge_list.out 90000000 Merged_edge_list

cd ../5_Matched_DegSigma
echo "Calculating the number of matched nodes, the average degree and the matched variance of the merged graph."
mono DA_matched_AvgDeg_Sigma.exe ../4_MatchMerge/Merged_edge_list > match_avg_deg_and_var_edge_list
cat match_avg_deg_and_var_edge_list

cd ../6_QMSampler
echo "Generating the input for QMSampler."
python3 QMSamplerInput.py $foursquaresize $flickrsize $((target/2)) $((target/2)) $target $eps
cat QMSamplerInput.txt
./QMSampler < QMSamplerInput.txt
cat output.txt
python3 next_num_node_and_edge.py $target ../../foursquare/edge_list ../../flickr/edge_list.out num_node_foursquare_${target}_iter1.txt num_node_flickr_${target}_iter1.txt

let current_sum_nodes=$current_num_node_foursquare+$current_num_node_flickr
cd ../..

let i=1
while [ "$current_sum_nodes" -lt $n -o "$i" -lt $iter ]
do
    let sampled_num_node_foursquare=$(cat QMSampler/6_QMSampler/num_node_foursquare_${target}_iter${i}.txt)
    let sampled_num_node_flickr=$(cat QMSampler/6_QMSampler/num_node_flickr_${target}_iter${i}.txt)

    echo "Start crawling FourSquare."
    python3 crawler.py -t foursquare -n $sampled_num_node_foursquare
    echo "The edge list of FourSquare is done."

    echo "Start crawling Flickr."
    python3 crawler.py -t flickr -n $sampled_num_node_flickr
    echo "The edge list of Flickr is done."

    echo "Now compress the IDs in the edge list of Flickr."
    python3 compress_id.py flickr/edge_list
    echo "Finish compressing. The compressed file is edge_list.out"

    echo "Combining the edge lists of FourSquare of iteration $((i-1)) and iteration ${i}."
    python3 combine.py foursquare/edge_list Merged_foursquare_iter$((i-1)) Merged_foursquare_iter${i}

    echo "Combining the edge lists of Flickr of iteration $((i-1)) and iteration ${i}."
    python3 combine.py flickr/edge_list.out Merged_flickr_iter$((i-1)) Merged_flickr_iter${i}

    python3 check_node_num.py Merged_foursquare_iter${i} Merged_flickr_iter${i}
    current_num_node_foursquare=$(cat current_node_info_of_Merged_foursquare_iter${i})
    current_num_node_flickr=$(cat current_node_info_of_Merged_flickr_iter${i})

    cd QMSampler/3_SingleNetwork_DegSigma
    echo "Calculating the average degree and variance of the graph crawled from FourSquare."
    mono SingleNetwork_avgDeg_Sigma.exe ../../Merged_foursquare_iter${i} > avg_deg_and_var_of_foursquare
    cat avg_deg_and_var_of_foursquare

    echo "Calculating the average degree and variance of the graph crawled from Flickr."
    mono SingleNetwork_avgDeg_Sigma.exe ../../Merged_flickr_iter${i} > avg_deg_and_var_of_flickr
    cat avg_deg_and_var_of_flickr

    cd ../4_MatchMerge
    mono MatchMerge.exe ../../match_list ../../Merged_foursquare_iter${i} ../../Merged_flickr_iter${i} 90000000 Merged_edge_list

    cd ../5_Matched_DegSigma
    echo "Calculating the number of matched nodes, the average degree and the matched variance of the merged graph."
    mono DA_matched_AvgDeg_Sigma.exe ../4_MatchMerge/Merged_edge_list > match_avg_deg_and_var_edge_list
    cat match_avg_deg_and_var_edge_list

    let current_sum_nodes=$current_num_node_foursquare+$current_num_node_flickr
    cd ../6_QMSampler
    echo "Generating the input for QMSampler."
    python3 QMSamplerInput.py $foursquaresize $flickrsize $current_num_node_foursquare $current_num_node_flickr $target $eps
    cat QMSamplerInput.txt
    ./QMSampler < QMSamplerInput.txt
    cat output.txt
    python3 next_num_node_and_edge.py $target ../../Merged_foursquare_iter${i} ../../Merged_flickr_iter${i} num_node_foursquare_${target}_iter$((i+1)).txt num_node_flickr_${target}_iter$((i+1)).txt
    cd ../..
    (( i++ ))
done

echo "End. Please check the merged file Merged_edge_list."