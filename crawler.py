#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import argparse
import string
import random
from os import path

from networkx import Graph
from requests import get
import twitter

from credentials import twitter_credentials, flickr_key, foursquare_credentials


def save_profile(target, user_id, user_name):
    with open(path.join(target, user_id), 'w') as f:
        f.write(user_name)


def save_graph(target, G, mapping):
    with open(path.join(target, 'vertex_mapping'), 'w') as f:
        f.write('\n'.join(['\t'.join(_) for _ in mapping.items()]))
    with open(path.join(target, 'vertex_list'), 'w') as f:
        f.write('\n'.join(list(G.nodes())))
    with open(path.join(target, 'edge_list'), 'w') as f:
        edges = [q[0] + ' ' + q[1] for q in G.edges()]
        f.write('\n'.join(edges))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(usage="-h for full usage")
    parser.add_argument('-t', '--target',
            help='supported targets including twitter, flickr, foursquare', required=True)
    parser.add_argument('-n', help='number of friends', type=int, required=True)
    parser.add_argument('--random-start',
            help="randomly pick the start", action='store_true', default=False)
    args = parser.parse_args()
    G = Graph()
    vertex_mapping = dict()

    if args.target not in ['twitter', 'flickr', 'foursquare']:
        sys.exit(1)

    if args.target == 'twitter':
        api = twitter.Api(**twitter_credentials)
        if args.random_start:
            while True:
                user_id = ''.join(random.choice(string.digits) for _ in range(8))
                try: api.GetUser(user_id)
                except: pass
                else: break
        else:
            user_id = '25073877'

        vertex_mapping[user_id] = api.GetUser(user_id).screen_name
        G.add_node(user_id)
        save_profile(args.target, user_id, api.GetUser(user_id).screen_name)
        while G.size() < args.n:
            try:
                neighbors = api.GetFriends(user_id=user_id)
                w_id = random.choice(neighbors).id
                w_neighbors = api.GetFriends(user_id=w_id)
                p = random.uniform(0, 1)
                if p <= len(neighbors)/len(w_neighbors):
                    G.add_edge(user_id, w_id)
                    user_name = api.GetUser(w_id).screen_name
                    save_profile(args.target, w_id, user_name)
                    user_id = w_id
                    vertex_mapping[user_id] = user_name
                    print('User: {}({})'.format(user_name, user_id))
            except KeyboardInterrupt:
                break
            except:
                print('Oops! Retrying...')
                continue
            finally:
                save_graph(args.target, G, vertex_mapping)

    if args.target == 'flickr':
        flickr_base = 'https://api.flickr.com/services/rest/?format=json&nojsoncallback=1'
        if args.random_start:
            for _ in range(10**4):
                user_id = ''.join(random.choice(string.digits) for _ in range(8)) + '@N06'
                print('Try ' + user_id)
                if get(flickr_base, params={'method':'flickr.urls.getUserProfile',
                        'api_key':flickr_key, 'user_id':user_id}).json()['stat'] == 'ok':
                    break
        else:
            user_id = '45166873@N06'

        print('Retrieving user name...')
        while True:
            response = get(flickr_base, params={'method':'flickr.people.getInfo', 'api_key':flickr_key,
                            'user_id':user_id}).json()
            if response['stat'] == 'ok':
                user_name = response['person']['username']['_content']
                print('username: ' + user_name )
                break

        vertex_mapping[user_id] = user_name
        G.add_node(user_id)
        save_profile(args.target, user_id, user_name)
        while G.size() < args.n:
            try:
                response = get(flickr_base, params={'method':'flickr.contacts.getPublicList',
                                'api_key':flickr_key, 'user_id':user_id}).json()
                if response['contacts']['total'] == 0:
                    user_id = random.choice(G.nodes())
                    continue

                neighbors = response['contacts']['contact']
                w_id = random.choice(neighbors)['nsid']
                w_neighbors = get(flickr_base, params={'method':'flickr.contacts.getPublicList',
                                'api_key':flickr_key, 'user_id':w_id}).json()['contacts']['contact']
                p = random.uniform(0, 1)
                if p <= len(neighbors)/len(w_neighbors):
                    G.add_edge(user_id, w_id)
                    user_name = get(flickr_base, params={'method':'flickr.people.getInfo', 'api_key':flickr_key,
                                    'user_id':user_id}).json()['person']['username']['_content']
                    save_profile(args.target, w_id, user_name)
                    user_id = w_id
                    vertex_mapping[user_id] = user_name
                    print('User: {}({})'.format(user_name, user_id))
            except KeyboardInterrupt:
                break
            except:
                print('Oops! Retrying...')
                continue
            finally:
                save_graph(args.target, G, vertex_mapping)

    if args.target == 'foursquare':
        foursquare_base = 'https://api.foursquare.com/v2/users/'
        if args.random_start:
            while True:
                user_id = ''.join(random.choice(string.digits) for _ in range(8))
                print('Try ' + user_id)
                if get(foursquare_base + user_id, params=foursquare_credentials).json()['meta'] == 200:
                    break
        else:
            user_id = '57867288'

        res = get(foursquare_base + user_id, params=foursquare_credentials).json()['response']['user']
        user_name = res['firstName'] + ' ' + res['lastName']
        vertex_mapping[user_id] = user_name
        G.add_node(user_id)
        save_profile(args.target, user_id, user_name)
        while G.size() < args.n:
            try:
                neighbors = get(foursquare_base + user_id + '/friends',
                        params=foursquare_credentials).json()['response']['friends']['items']
                w = random.choice(neighbors)
                w_id = w['id']
                w_neighbors = get(foursquare_base + w_id + '/friends',
                        params=foursquare_credentials).json()['response']['friends']['items']
                p = random.uniform(0, 1)
                if p <= len(neighbors)/len(w_neighbors):
                    G.add_edge(user_id, w_id)
                    user_name = w['firstName'] + ' ' + w['lastName']
                    save_profile(args.target, w_id, user_name)
                    user_id = w_id
                    vertex_mapping[user_id] = user_name
                    print('User: {}({})'.format(user_name, user_id))
            except KeyboardInterrupt:
                break
            except:
                print('Oops! Retrying...')
                continue
            finally:
                save_graph(args.target, G, vertex_mapping)
